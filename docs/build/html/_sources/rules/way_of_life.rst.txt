.. _way_of_life:

====================
LA SUPACHARTE DE VIE
====================

**La présente Charte de vie et ses commandements ont été établis en l’an 13 après Supermoine.**

.. contents:: Les 8 saints commendements de Supermoine

Commandement n°1
----------------
	Je respecterai le RÈGLEMENT du cloître SUPAMONKS disponible ici.

Commandement n°2
----------------
	**Verset 1**
		Je respecterai mes frères et mes soeurs, leurs personnalités, leurs choix vestimentaires, leurs goûts musicaux et culturels en général, leurs orientations politiques, sexuelles et religieuses. Supermoine aime son prochain, Supermoine ouvre ses bras à tous. Il peut aussi réprimander sévèrement des comportements inappropriés, racistes, homophobes, misogynes, etc. qui ne sauraient être tolérés au sein du cloître.

	**Verset 2**
		Je n’hésiterai pas à informer poliment l’un des évêques, l’un de mes frères ou l’une de mes soeurs si son comportement ou ses mots à mon égard me déplaisaient. Supermoine sait qu’entre l’humour et l’irrespect la frontière est parfois ténue. Si le comportement inapproprié à mon égard ne devait changer malgré ma demande, je me tournerai alors vers les évêques pour leur demander de régler la question. Si le comportement ou les mots déplacés étaient le fait de l’un des évêques je pourrai me tourner vers les autres évêques et/ou mes frères et soeurs pour demander leur soutien.

Commandement n°3
----------------
	**Verset 1** : Je respecterai mon poste de travail. 
		- Je ne laisserai pas s'accumuler sur mon bureau un nombre inadéquate d’objets n’ayant rien à voir avec mon travail (Supermoine autorise la personnalisation des postes avec des objets personnel dans la mesure du raisonnable).
		- J’éteindrai mon PC tous les soirs avant de partir du cloitre.
		- Je ne jetterai pas de denrées organiques périssables type peaux de bananes, sachets de thé, ailes de poulet dans ma poubelle de bureau qui est destinée à accueillir du papier. Je suis invité à jeter ce type de denrées dans la poubelle du réfectoire.
		- Je n’abimerai pas mon ordinateur et tous les objets qui y sont connectés volontairement ou par négligence (par exemple en renversant du liquide dessus).
		- Lors de mon dernier jour de présence prévu dans le cloître, je veillerai à ce que mon poste se retrouve dans le même état que lorsqu’il m’a accueilli, en récupérant mes affaires personnels (et même dans les tiroirs/placards).

	**Verset 2** : Je respecterai les lieux d’aisance.
		- Je n’urinerai pas à côté de la cuvette sachant qu’il est assez simple d’uriner au centre de cette dernière.
		- Si toutefois j’urinais à côté pour une mystérieuse raison ou une autre, j’utiliserai un papier pour nettoyer les traces.
		- Si mes matières fécales étaient amenées à laisser des traces sur la cuvette, j’utiliserai le balai prévu à cet usage pour les faire disparaître.
		- Je ne jeterai aucun objet dans le puit de l’abandon à l’exception de mes urines, mes matières fécales le papier toilette. Tout autre objet doit être jeté dans les poubelles prévues à cet effet.


	**Verset 3** : Je respecterai le réfectoire et les lieux de vie.
		- Je nettoierai le plan de travail de la cuisine après l’avoir utilisé pour préparer ma pitance.
		- Si le four ou le four micro-onde venaient à subir des éclats provenant de ma pitance je les nettoierais.
		- Après avoir consommé ma pitance je ferai ma vaisselle (assiette, verre, couverts, casserole, poêle, etc.).
		- Après avoir consommé ma pitance je ne laisserai pas traîner les éventuels condiments (sel, poivre, sauce, vinaigre, huile, épices, etc.) sur la table du réfectoire mais les remettrai à leur place.
		- Après avoir débarrassé ma place, je passerai l’éponge sur l’espace de table utilisé.
		- Supermoine informe qu’il ne sert à rien de poser des déchets sur une poubelle déjà pleine à craquer (“C’est complètement con et énervant” sic.). Je fermerai la poubelle pleine, la stockerai près du bac à poubelle dans la cuisine et mettrai un nouveau sac dans le dit bac.
		- Je ne laisserai rien trainer dans le vestibule d’entrée.
		- Je rangerai ma bicyclette à la cave car personne n’a jamais considéré qu’un vélo pouvait constituer une décoration agréable à l’oeil.
		- Sur le parvis du cloître, je jetterai mes mégots dans les cendriers prévus à cet usage.

Commandement n°4
----------------
	Je veillerai à ne pas laisser le cloître sans défense face aux brigands (si je suis le dernier à quitter le cloître je dois vérifier la fermeture des fenêtres et portes, mettre l’alarme et fermer à clés).

Commandement n°5
----------------
	Je respecterai les horaires d’entrée et sortie pour le bon fonctionnement du cloître et dans le respect de mes frères et soeurs qui font leurs meilleurs efforts pour les respecter.

Commandement n°6
----------------
	Le cloître s’inscrit dans un projet urbain plus large qui implique un voisinage. Je ferai mes meilleurs efforts pour me montrer respectueux et avenant à l’égard de ce voisinage. Supermoine le dit toujours “Dire “Bonjour” et sourire n’a jamais tué personne  même si l’enfoiré en face de répond pas”.

Commandement n°7
----------------
	je serai bon citoyen, ne laisserai pas l’eau couler plus que nécessaire ni de lumières allumées dans une pièce si je suis le dernier à la quitter. Supermoine aime sa planète. J’adhére à une charte éco-responsable que je trouve ici : :ref:`eco_rules`

Commandement n°8
----------------
	Le mercredi quand la soeur Mariette me parlera, je viderai le réfrigérateur ou courrai le risque de voir s’égayer par monts et par vaux les restes de ma pitance.

Commandement n°9
----------------
	Je ferai mes meilleurs efforts pour toujours être rayonnant, merveilleux, bienveillant, doux. Un parfait petit poney s’égayant dans un champ de pâquerettes que ce soit au sein du cloître ou lorsque je le représente dans la société civile.