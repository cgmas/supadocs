=============
Les SupaFaces
=============
**Présentation des permanents**

.. image :: images/faces/2018Card_Supa_008.jpg

.. contents::

Les 4 Bishops
--------------------
*Les fondateurs*

.. _kast:

Julien "Kast" Bagnol
____________________
	*Directeur du studio*
	*CEO*

	.. image :: images/faces/2018Card_KAST.jpg
		:height: 620
		:width: 500

	Sous ses airs un peu endormi, il est très calinou, et assez discret. Si tu veux lui faire plaisir, apporte lui du fromage (du bon, qui pue bien !).

.. _peyo:

Pierre "Peyo" de Cabissole
__________________________
	*Directeur des productions*
	*COO*

	.. image :: images/faces/2018Card_PEYO.jpg
		:height: 620
		:width: 500

	Il fait beaucoup de bruit, en même temps si tu passes du Francis Cabrel et du Céline Dion, t’as cherché !

.. _xii:

Florian "XII" Landouzy
______________________
	*Directeur d'animation*
	*CAO*

	.. image :: images/faces/2018Card_XII.jpg
		:height: 430
		:width: 620

	Il a une longue barbe, et une longue bite (à ce qui paraît…) en plus d’être un vrai pédagogue et passionné de l’animation 3D et 2D.

.. _zwib:

Sébastien "Zwib" Ho
___________________
	*Directeur technique*
	*CTO*

	.. image :: images/faces/2018Card_ZWIB.jpg
		:height: 620
		:width: 500

	Il fait pas mal de choses dans son coin très diverses, C’est technique, ça à l’air passionnant.

Les Abbots
----------

.. _marlene:

Marlène "Winnie Joe" Ranchon
____________________________
	*Directrice de production*

	.. image :: images/faces/2018Card_MARLENE.jpg
		:height: 620
		:width: 500

	C'est maman.

.. _saf:

Quentin "Safran" Juillard
_________________________
	*Directeur de production*

	.. image :: images/faces/2018Card_QUENTIN.jpg
		:height: 620
		:width: 500

	"Non mais moi, ça me va"

.. _seb:

Sébastien "Sled" Le Divenah
___________________________
	*Directeur Arstitique*

	.. image :: images/faces/2018Card_SEB.jpg
		:height: 620
		:width: 500

	Oooooooh des zoulis dessiiiiiiiins !!

.. _youcef:

Youcef Safraoui
_______________
	*Directeur financier*

	.. image :: images/faces/2018Card_YOUCEF.jpg
		:height: 620
		:width: 500

	Son poste fait peur, (peut-être que lui aussi ?) mais il fait des très bons tajines ! (c’est pas raciste, c’est vrai !)

.. _valou:

Valentine "Valou" de Blignière
______________________________
	*Directrice du développement*

	.. image :: images/faces/2018Card_VALOU.jpg
		:height: 620
		:width: 500

	Elle rencontre beaucoup de gens, et elle arrive à vendre du rêve ! Grâce à elle on a des projet. Et elle est super forte au Yoga !

.. _dee:

Damien "Dee" Coureau
____________________
	*Directeur R&D*

	.. image :: images/faces/2018Card_DEE.jpg
		:height: 620
		:width: 500

	Il a plein d’écran, tournés dans tous les sens, c’est écrit tout bizarre dessus, mais il est sympa tu verras !

.. _max:

Maxime "Max" Caron
__________________
	*CG Supervisor*

	.. image :: images/faces/2018Card_MAX.jpg
		:height: 620
		:width: 500

	Toujours à l'affût de la mode Parisienne vestimentaire, il ne se déplace jamais sans sa gourde en métal.

.. _pink:

Nicolas "Pink" Perraguin
________________________
	*CG Supervisor*

	.. image :: images/faces/2018Card_PINK.jpg
		:height: 620
		:width: 500

	L’employé du mois tous les mois.

.. _lam:

Lam Le Thanh
____________
	*CG Supervisor*

	.. image :: images/faces/2018Card_LAM.jpg
		:height: 620
		:width: 500

	McGiver vietnamien, fan d’escalade.

.. _claire:

Claire Besson
_____________
	*CG Supervisor*

	.. image :: images/faces/2018Card_CLAIRE.jpg
		:height: 620
		:width: 500

	Fan de puzzle et incapable de brutaliser une mouche.

.. _simon:

Simon Reynaud
_____________
	*Rigging Supervisor*

	.. image :: images/faces/2018Card_SIMON.jpg
		:height: 620
		:width: 500

	Toujours la queue de cheval, et toujours manger bio !

.. _laura:

Laura Saintecatherine
_____________________
	*Compositing Supervisor*

	.. image :: images/faces/2018Card_LAURA.jpg
		:height: 620
		:width: 500

	Un ange qui polish tes images comme personne !

.. _pel:

Pierre-Edouard "Pel" Landes
___________________________
	*PhD Dev*

	.. image :: images/faces/2018Card_PEL.jpg
		:height: 620
		:width: 500

	Le docteur du studio. A tendance à avoir faim à 15h...

.. _amar:

Amar Bouacheria
_______________
	*Directeur de ULF*

	.. image :: images/faces/2018Card_AMAR.jpg
		:height: 620
		:width: 500

	C'est le client alors soit sage !

Les monks
---------

**C'est vous !**