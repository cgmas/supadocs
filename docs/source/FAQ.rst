.. _FAQ:

***
FAQ
***

Poste de travail
================

Je n’ai pas de tablette graphique sur mon poste et j’en ai besoin, comment je travaille?
----------------------------------------------------------------------------------------
	- Avec une feuille et un crayon… Plus sérieusement, adresse toi à :ref:`Zwib <zwib>` et si pas disponible, à :ref:`Safran <saf>`.

J’ai un souci avec un logiciel / window / mon driver, j’ai déjà cherché sur google et je ne comprends pas, que fais-je pour travailler ?
----------------------------------------------------------------------------------------------------------------------------------------
	- Adresse toi à :ref:`Zwib <zwib>`, et si ce dernier est absent, essaie avec :ref:`Safran <saf>`. Si tu es amené à changer de poste, demande à l’équipe de prod à quel poste tu peux t’installer.

Je n’ai pas photoshop sur mon PC, comment je fais ?
---------------------------------------------------
	- Ouvre Krita, tu verras c’est pas mal !


Administration / RH
===================

Sur mon contrat il est marqué une durée inférieure à laquelle ma mission est prévue, pourquoi ? Est-ce normal ?
---------------------------------------------------------------------------------------------------------------
	- Lorsque ton contrat est établi, pour anticiper des annulations opportunes de production, nous garantissons donc une durée de jours payés minimale dans le mois (souvent la moitié de ton temps prévu dans le mois), ainsi même si la production se terminait à ton deuxième jour de travail, si ton contrat t’assures 5 jours chez nous, tu seras payé 5 jours. A la fin du mois, un avenant à ton contrat te sera desservi avec une correction (forcément supérieure) du nombre de jours exact que tu auras travaillés chez nous.

On m’a donné une fiche de paie et un avenant au contrat, alors que ma mission se termine dans une semaine / 3 jours etc, pourquoi ?
-----------------------------------------------------------------------------------------------------------------------------------
	- Si nous sommes en fin de mois, c’est tout à fait normal, un intermittent du spectacle a une fin de contrat tous les mois. Ta semaine ou tes jours qui restent seront payés pour le mois suivant. Pour plus d’information sur l’intermittence réfère toi au doc concerné ! Si nous ne sommes pas en fin de mois, ce n’est pas normal, tu peux t’adresser à la personne qui t’a recruté(e) pour en parler.


J’ai entendu dire qu’on me remboursera mes titres de transports à la hauteur de 50%, et je n’ai rien sur ma fiche de paie, pourquoi ?
-------------------------------------------------------------------------------------------------------------------------------------
	- A la fin de chaque mois, tu peux te présenter, muni(e) de ta facture d’abonnement mensuel aux transports en communs, auprès de :ref:`l’équipe administrative <youcef>`, nous ferons un remboursement au prorata de tes jours travaillés au sein du studio dans le mois.


Je suis intermittent et j’aimerais que mon employeur me rembourse 50% de la mutuelle (assurance santé) de l’entreprise si j’y souscris, y ai-je bien le droit ?
---------------------------------------------------------------------------------------------------------------------------------------------------------------
	- Une mutuelle (assurance santé) est obligatoire pour tous les salariés au régime général, en tant qu’intermittent tu peux souscrire à la mutuelle de l’entreprise, mais Supamonks n’est pas ton employeur unique, tu ne bénéficieras donc pas de la prise en charge des 50%. Un intermittent n’est pas obligé de souscrire à la mutuelle de l’entreprise, et il n’est d’ailleurs pas obligé de souscrire à une mutuelle (assurance santé) tout court.


C’est quoi le truc des jours fériés à Supamonks ?
-------------------------------------------------
	- En tant qu’intermittent du spectacle tu es susceptible de travailler les week end et les jours fériés. A Supamonks les jours fériés sont des jours habituels de travail s’ils tombent du Lundi au Vendredi. La rémunération est la même qu’un jour travaillé.
	- Un stagiaire n’est pas obligé de se rendre à Supamonks un jour férié, mais s’il le souhaite, le studio est ouvert, et sa journée sera comptabilisée.
	- Les seuls jours fériés non travaillés à Supamonks sont le 1er Mai, le 1er Janvier, et le 25 Décembre.

Y’a-t-il des congés exceptionnelle pour Noël ?
----------------------------------------------
	- Supamonks ferme ses locaux la semaine entre Noël et nouvel an.

Je n’ai pas reçu de tickets restaurant, pourquoi ?
--------------------------------------------------
	- Supamonks ne fournit pas (encore !) de tickets restaurant.

J’ai changé d’adresse postale / je me suis marié(e) / j’ai changé de compte en banque etc… pendant ma mission chez vous, à qui dois-je le notifier ?
----------------------------------------------------------------------------------------------------------------------------------------------------
	- :ref:`L’équipe administrative <youcef>` est là pour ça !

J’ai travaillé sur un projet, je peux mettre quoi sur ma bande démo ? Et quand ?
--------------------------------------------------------------------------------
	- A chaque fin de projet, n’hésite pas à demander au prod responsable du projet, le niveau de confidentialité de ce que tu viens de finir, et de quand tu pourras le diffuser dans ta bande démo. Dans tous les cas, si tu vois la production finie sur le facebook ou le vimeo de Supamonks, n’hésite pas à la mettre également sur ta bande démo, c’est que c’est 100% sûr !

Je prévois de rester tard pour travailler, à quel tarif seront payées mes heures supplémentaires ?
--------------------------------------------------------------------------------------------------
	- Regarde les modalités de ton contrat, en général à Supamonks les heures supplémentaires ne sont pas payées. Si tu te sens en retard dans la tâche qui t’es demandée, essaie d’en informer au plus tôt le prod en charge du projet pour pallier un éventuel retard. Et rentre chez toi à 19h !

Vie au studio
=============

J’entends parler de la Suparésidence, qu’est-ce exactement ? Je travaille à Supamonks, et j’aimerais y participer, comment ?
----------------------------------------------------------------------------------------------------------------------------
	- Va voir le concept de la :ref:`Suparésidence <suparez>` !
	- Tu travailles sur une production actuellement au studio et tu es sollicité, ou aimerais participer à la Suparésidence en cours, c’est possible ! Mais en dehors de tes horaires de travail à Supamonks, le matin, le soir, le temps de midi, le week end etc… 

Pourquoi :ref:`Peyo <peyo>` a une béquille ?
--------------------------------------------
	- Pose-lui la question !

Pourquoi :ref:`Lan12 <xii>` a une longue barbe ?
------------------------------------------------
	- Pour égaler la taille de sa bite !

J’entends beaucoup de blagues de caca, de bite, et de pipi c’est normal ?
-------------------------------------------------------------------------
	- Plus ou moins, si c’est ta came, joins-toi aux blagues de beauf ! Si ça te gêne, n’hésite pas à le notifier aux principaux intéressés. Si ça ne change rien, tu peux te confier à un des monks permanents qui se chargera de faire passer le message.

On m’a posé une ou plusieurs questions personnelles, je ne souhaite pas y répondre, comment réagir ?
----------------------------------------------------------------------------------------------------
	- Tu peux tout à fait refuser de répondre, si la personne est insistante, renvoie-lui la question, ou change de sujet. Je t’invite à venir en discuter avec un des monks permanents par la suite pour faire remonter l’information, on ne te veut que du bien !

J’aimerais imprimer quelque chose au studio est-ce possible ?
-------------------------------------------------------------
	- Bien sûr ! Mets ton document quelque part sur le réseau, et demande gentiment si c’est possible à :ref:`Peyo <peyo>` s’il est sur son poste. Sinon utilise le (son poste, pas Peyo) et imprime sur l’imprimante de son bureau.

Qui est le loup garou ?
-----------------------
	- C’est Morgan, c’est toujours Morgan.

