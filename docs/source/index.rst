.. SupaDocs documentation master file, created by
   sphinx-quickstart on Mon Sep  3 11:04:40 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: SUPAMONKS-LOGO_black.png

###########################
Bienvenue sur la SupaDocs !
###########################

.. raw:: html

	<video width="700" height="394" autoplay="true" loop muted>
		<source src="_static/supareel.mp4">
	</video>

Ici, tu pourras trouver des tonnes d'informations super utiles !
Ci-dessous tu trouvers un index qui te permettras de naviguer dans ce flow incroyable d'informations !

Supamonks sur le web :
   - Le site : https://www.supamonks.com
   - Le vimeo : https://vimeo.com/supamonks
   - Le facebook : https://www.facebook.com/supamonks/

.. toctree::

   Les chartes <rules_index>
   Les SupaFaces <trombi>
   Les tutos <tuts_index>
   Les prods <prods_index>
   FAQ

.. Indices and tables :
..    * :ref:`genindex`
..    * :ref:`modindex`
..    * :ref:`search`