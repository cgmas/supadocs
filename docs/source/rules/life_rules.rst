.. _life_rules:

===================
RÈGLEMENT INTÉRIEUR
===================

.. contents:: Index

.. _rules_titre_i:

Titre I - PREAMBULE
===================

Article 1 – Objet
-----------------
| Le présent règlement intérieur relève d’une obligation juridique au titre des articles L1311-1 et L1311-2 du Code du travail.
| Il est établi dans le respect du formalisme prescrit aux articles L1321-1 et suivants du Code du travail.
| Le même formalisme s’applique en cas de modification ou de retrait des clauses dudit règlement.
| Le règlement intérieur est destiné à organiser la vie dans l’entreprise dans l’intérêt de tous. Il vise tant les droits et les devoirs des salariés que les obligations de l’employeur.
| Conformément aux dispositions légales, il :

    - rappelle les règles générales et permanentes relatives à la discipline, les procédures et sanctions disciplinaires ainsi que les dispositions relatives aux droits de la défense des salariés de SUPAMONKS
    - rappelle les dispositions relatives à l’interdiction de toute pratique de harcèlement sexuel ou moral et de discrimination,
    - fixe les mesures d’application de la réglementation en matière d’hygiène, de santé et de sécurité dans l’entreprise et les conditions dans lesquelles les personnels peuvent être appelés à participer, à la demande de l’employeur, au rétablissement de conditions de travail protectrices à la santé et à la sécurité dès lors qu’elles apparaîtraient compromises.

| Le règlement intérieur permet également d’encadrer le pouvoir de contrôle et de discipline de l’employeur. Les mesures prises dans l’application des dispositions du règlement intérieur le sont de manière graduée et proportionnée toujours dans l’esprit d’organiser la vie d’entreprise et non à des fins de sanction systématique des personnes.
| La hiérarchie veille à son application.
 
 
Article 2 - Personnel concerné et Champ d’application
-----------------------------------------------------
| 2.1 Personnel concerné
| Parce qu’il est destiné à organiser la vie dans l’entreprise dans l’intérêt de tous, sauf mention contraire, toutes les dispositions du règlement intérieur relatives à la discipline générale (:ref:`rules_titre_ii`) et aux règles générales et permanentes d’hygiène, de santé et de sécurité (:ref:`rules_titre_iii`) s’appliquent à tous les personnels travaillant dans l’entreprise (employés à durée indéterminée ou déterminée, en contrat statutaire ou non, salariés d’entreprises extérieures, intérimaires, stagiaires, présents dans l’entreprise et de façon générale à toute personne qui travaille dans l’entreprise qu’elle soit liée ou non par un contrat de travail) qui effectuent un travail sur un des sites de SUPAMONKS et pour le compte de celle-ci.
| Ces catégories de personnel sont dénommées « les personnels » dans le présent règlement intérieur.
| Les salariés d’entreprises extérieures, les intérimaires et les stagiaires n’étant pas sous contrat de travail avec SUPAMONKS, ne sont pas soumis à son pouvoir disciplinaire.

| 2.2 Champ d’application
| Le présent règlement intérieur s’applique à tous les salariés de l’entreprise en quelque endroit qu’ils se trouvent. Il se substitue à tout autre règlement ou mesure locale adoptée antérieurement. Il s’applique également à toute personne présente, en quelque endroit qu’elle se trouve dans l’entreprise. 


.. _rules_titre_ii:

Titre II - REGLES GENERALES DE VIE DANS L’ENTREPRISE - DISCIPLINE GENERALE
==========================================================================

Article 3 - Horaire de travail
------------------------------
| Les salariés sont tenus de respecter leurs durées de travail et leurs horaires, tels que prévus par les accords collectifs et/ou les décisions unilatérales de l’employeur portant sur le temps de travail, applicables dans l’entreprise ou l’établissement d’appartenance, que ceux-ci soient collectifs ou individuels.
| Pour rappel les jours travaillés sont les lundi-mardi-mercredi-jeudi-vendredi
| Les horaires sont : 10h-13h / 14h-19h
| La hiérarchie est garante du respect de ces horaires.
| Pour les cadres sous convention forfait jour, ils suivent les dispositions particulières prévues dans leur convention.



Article 4 - Retards, absences
-----------------------------
| La hiérarchie doit être prévenue par tous moyens dès le début d’une absence.
| Toute absence prévisible pour motif personnel doit être préalablement autorisée par le responsable hiérarchique.
| Cette autorisation est subordonnée au respect d’un délai de prévenance de 48 heures. Cette obligation ne vise pas les situations imprévisibles ou de force majeure qui devront être portées à la connaissance de la hiérarchie dans les plus brefs délais.
| Les sorties pour convenance personnelle pendant les heures de travail doivent être exceptionnelles ; elles doivent être subordonnées à une autorisation expresse de la hiérarchie.
| Les salariés amenés à subir un retard devront faire connaître dans les meilleurs délais à leur hiérarchie les motifs de ce retard.
| S’agissant des représentants du personnel, les conditions d’utilisation de leurs crédits d’heures sont régies par des dispositions conventionnelles.
 

Article 5 - Maladie et accident
-------------------------------
| Pour les salariés statutaires, les dispositions de l’article 22 § 6 du Statut national du personnel des IEG sont applicables. L’ensemble des dispositions du Statut national du personnel des IEG est consultable à partir de l’Intranet de SUPAMONKS.
| Pour les salariés non statutaires, ce sont les seules dispositions du Code du travail et du Code de la sécurité sociale qui sont applicables.
| Le salarié doit avertir sa hiérarchie dans les plus brefs délais et produire dans un délai de 48 heures le certificat médical attestant son arrêt et indiquant la durée de son indisponibilité.
| En cas de prolongation de l’arrêt de travail, un délai de 48 heures doit être également respecté pour justifier la prolongation.



Article 6 - Accès à l’entreprise
--------------------------------
| 6.1 Les personnels n’ont accès aux lieux de travail que pour l’exécution de leur contrat de travail ou de leur prestation, exception faite lorsqu’ils peuvent se prévaloir :

	- d’une disposition légale ou conventionnelle relative aux droits de la représentation du personnel ou des syndicats,
	- d’une autorisation de la hiérarchie ou de toute personne habilitée.

| L'entrée et la sortie des personnels s'effectuent uniquement par les accès prévus à cet effet ; les sorties de secours ne sont utilisées qu'en cas d'évacuation.

| 6.2 Les personnels sont tenus de respecter les procédures et consignes relatives à l’accès et à la circulation des personnes et des véhicules à l’intérieur des différents sites. Ces dispositifs concourent à protéger certains sites contre toute intrusion de personne non autorisée.
| Il est interdit aux personnels d'introduire ou de faire introduire dans les locaux de SUPAMONKS des personnes étrangères à l’entreprise, sans raison de service et sauf dispositions légales particulières ou autorisation de la hiérarchie; il en est de même pour les animaux. Cette dernière disposition ne concerne pas les animaux qui accompagnent les personnes en situation de handicap.
| L'accès aux sites est interdit aux enfants mineurs non accompagnés d'un responsable (parent, encadrant scolaire ou d'activités sociales), hors stage scolaire.

| 6.3 Il peut être procédé à l’installation de vidéo surveillance dans le but notamment de lutter contre la malveillance ainsi que de prévenir des atteintes à la sécurité des personnes et des biens, dans le respect des dispositions représentatives du personnel et de la nécessaire information des salariés. En cas d’enregistrement et de conservation des images des salariés, l’installation de caméras de vidéosurveillance doit faire l’objet préalablement :

	- D’une consultation des IRP compétentes,
	- D’une information individuelle écrite aux salariés.

| Le site sous vidéo surveillance fait l’objet d’une signalétique. Conformément à la législation en vigueur, les enregistrements ne pourront être conservés que dans un délai d’un mois maximum. 

Article 7 - Usage des locaux de l’entreprise
--------------------------------------------
| 7.1 Les locaux de l’entreprise, en dehors de ceux dédiés aux activités sociales et syndicales, sont réservés exclusivement aux activités professionnelles des personnels, en dehors de toutes activités personnelles.
| Il est interdit notamment :

	- d’introduire dans les lieux de travail des objets et des marchandises destinés à y être vendus sans autorisation expresse de l’employeur, excepté toutes activités sociales,
	- d’utiliser pour son propre compte, sans autorisation, les locaux, les aires de stockage ou de stationnement, appartenant à l’entreprise,
	- d’introduire des armes blanches, des armes à feu même d’autodéfense,
	- de faire circuler, sans autorisation, des tracts, pétitions, listes de souscription ou de collecte ; seules la collecte des cotisations syndicales et la diffusion des publications et tracts syndicaux peuvent être faits sans autorisation managériale et dans les conditions prévues par la législation et les accords collectifs en vigueur dans l’entreprise,
	- de diffuser et d'afficher dans l'enceinte des différents sites de l’entreprise, des publications, tracts, pétitions à caractère politique, religieux ou sexuel, d'organiser des réunions politiques ou religieuses, de diffuser ou d’afficher des messages portant atteinte à la dignité humaine ou à la moralité. Cette interdiction vaut également pour la diffusion par support informatique.

| 7.2 L’affichage sur les murs des locaux collectifs est interdit en dehors des panneaux muraux d’information dédiés à la Direction et aux organisations sociales et syndicales, des informations et des consignes de sécurité ; les affiches ou les notes de service régulièrement apposées ne doivent pas faire l’objet de détériorations (lacération, destruction, falsification ou annotation).
| L’employeur doit veiller à ce que l’affichage légal soit mis en œuvre et maintenu en bon état.
 
 
Article 8 - Usage des biens et des ressources appartenant à l’entreprise
------------------------------------------------------------------------
| 8.1 SUPAMONKS met à la disposition des personnels des biens et des matériels pour l’exécution des activités. SUPAMONKS est tenu de s’assurer que ces biens et ces matériels sont conformes et en bon état ; il convient donc que SUPAMONKS en organise les contrôles réglementaires périodiques.

| 8.1.1 Utilisation des matériels
| Les personnels sont tenus de conserver en bon état (hors usure naturelle), par un usage normal et professionnel, tous les matériels mis à leur disposition en vue de l’exécution des activités (lles équipements techniques, les mobiliers et les outillages, les matériels informatiques et les systèmes d’information, les fournitures de toutes sortes), et de veiller à éviter toute dégradation, vol ou détournement.
| Le prêt de matériel aux personnels à titre privé doit être formalisé par écrit entre l’entreprise et l’utilisateur.


Article 9 - Restitution des biens en cas de mutation ou de cessation d’activité
-------------------------------------------------------------------------------
| 9.1 En cas de mutation ou de cessation de son contrat de travail, tout salarié doit, avant de quitter l’entreprise, restituer tous les biens et les matériels, les travaux, les dotations en équipements et les documents en sa possession qui appartiennent à l’entreprise. Sont notamment compris dans ces biens les clefs, le matériel téléphonique et le matériel informatique.

| 9.2 Tous les travaux d’études, les documents et les correspondances, sous quelque forme que ce soit (papier, logiciel, support électronique...), qui peuvent être confiés aux salariés ou que ceux-ci pourraient établir dans l’exercice de leurs fonctions restent la propriété exclusive de SUPAMONKS.
| Ils ne peuvent en aucun cas être reproduits pour des usages étrangers à cette activité sans un accord préalable d’un représentant habilité de cette entreprise.
 

Article 10 - Cadre général d’exécution des activités professionnelles
---------------------------------------------------------------------
| Les salariés de SUPAMONKS sont, s’agissant des obligations générales découlant du contrat de travail, soumis aux dispositions du statut national du personnel et à ses notes d’application ainsi qu’aux dispositions du présent article du règlement intérieur.

| 10.1 Dans l’exécution des tâches qui leur sont confiées, ils doivent se conformer aux instructions qui leur sont données par leur hiérarchie, ou le représentant hiérarchique dénommé en cas d’astreinte, ainsi qu’aux documents et procédures de travail en vigueur et portés à leur connaissance. En dehors des dispositions prévues par le droit de retrait, ils ne peuvent se prévaloir d’une quelconque raison, notamment politique ou religieuse, pour s’y soustraire.

| 10.2 Les personnels, dans le cadre des fonctions qui leur sont confiées, ont des obligations de confidentialité, de réserve et de préservation touchant de manière générale les informations dont ils sont destinataires et dont la divulgation pourrait porter préjudice à l’entreprise.
| Le patrimoine immatériel de SUPAMONKS (images, expertises, savoir-faire, marques, logiciels) doit être protégé en toute circonstance et ne doit pas faire l’objet de communication ou de destruction. En cas de doute sur la possibilité de transmission d’une information, le personnel doit en référer à sa hiérarchie directe.



Article 11 - Comportement et attitude
-------------------------------------
| 11.1 Les agressions et les menaces physiques ou morales, ainsi que les propos injurieux ou humiliants exprimés à l'égard de quiconque constituent des agissements qui peuvent être qualifiés de fautifs et passibles des sanctions prévues à l’article 15 du présent règlement.

| 11.2 Dans l’intérêt légitime de l’entreprise et afin de préserver une cohabitation harmonieuse et professionnelle entre les personnels, les pouvoirs publics, les clients et les autres entreprises partenaires, tous les personnels se doivent d’être courtois et de veiller à ne pas tenir des propos discriminatoires.


Article 12 - Tenue vestimentaire
--------------------------------
| Chaque salarié est tenu d’adopter au travail une tenue appropriée à son activité et conforme aux usages professionnels.

 
Article 13 - Interdictions du harcèlement moral ou sexuel
---------------------------------------------------------
| Le harcèlement moral ou sexuel est susceptible d’être exercé entre des personnes présentant ou non un rapport hiérarchique entre elles.

| 13.1 Le harcèlement moral
| Les articles L.1152-1 à 3 du Code du travail indiquent que :

	- « Aucun salarié ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation de ses conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d’altérer sa santé physique ou mentale ou de compromettre son avenir professionnel »,
	- « Aucun salarié, aucune personne en formation ou en stage ne peut être sanctionné, licencié ou faire l’objet d’une mesure discriminatoire, directe ou indirecte, notamment en matière de rémunération, de formation, de reclassement, d’affectation, de qualification, de classification, de promotion professionnelle, de mutation ou de renouvellement de contrat pour avoir subi ou refusé de subir des agissements répétés de harcèlement moral ou pour avoir témoigné de tels agissements ou les avoir relatés »,
	- « Toute rupture du contrat de travail intervenue en méconnaissance des dispositions des articles L.1152-1 et -2, toute disposition ou tout acte contraire est nul ».

| L’article L.1152-4 du Code du travail indique par ailleurs que « l’employeur prend toutes les dispositions nécessaires en vue de prévenir les agissements de harcèlement moral et d’y mettre un terme ».
| L’article L.1152-5 du Code du travail précise enfin que « tout salarié ayant procédé à des agissements de harcèlement moral est passible d’une sanction disciplinaire » (Cf. article 15).

| 13.2 Le harcèlement sexuel
| Les articles L.1153-1 à -4 du Code du travail énoncent que :

	- « Aucun salarié ne doit subir des faits :
		- Soit de harcèlement sexuel, constitué par des propos ou comportements à connotation sexuelle répétés qui soit portent atteinte à sa dignité en raison de leur caractère dégradant ou humiliant, soit créent à son encontre une situation intimidante, hostile ou offensante,
		- Soit assimilés au harcèlement sexuel, consistant en toute forme de pression grave, même non répétée, exercée dans le but réel ou apparent d’obtenir un acte de nature sexuelle, que celui-ci soit recherché au profit de l’auteur des faits ou au profit d’un tiers ».
	- « Aucun salarié, aucune personne en formation ou en stage, aucun candidat à un recrutement, à un stage ou à une formation en entreprise ne peut être sanctionné, licencié ou faire l’objet d’une mesure discriminatoire, directe ou indirecte, notamment en matière de rémunération, de formation, de reclassement, d’affectation, de qualification, de classification, de promotion professionnelle, de mutation ou de renouvellement du contrat pour avoir subi ou refusé de subir des faits de harcèlement sexuel tels que définis à l’article

| L.1153-1, y compris le cas mentionné au 1° de cet article, si les propos ou comportements n’ont pas été répétés »,

	- « Aucun salarié, aucune personne en formation ou en stage ne peut être sanctionné, licencié ou faire l’objet d’une mesure discriminatoire pour avoir témoigné de faits de harcèlement sexuel ou pour les avoir relatés »,
	- « Toute disposition ou tout acte contraire aux dispositions des articles L.1153-1 à L.1153-3 est nul »

| L’article L.1153-5 du Code du travail indique par ailleurs que « l’employeur prend toutes les dispositions nécessaires en vue de prévenir les agissements de harcèlement sexuel et d’y mettre un terme ».
| L’article L.1153-6 du Code du travail précise enfin que « tout salarié ayant procédé à des agissements de harcèlement sexuel est passible d’une sanction disciplinaire » (Cf. article 15).
 

Article 14 - Interdiction de la discrimination
----------------------------------------------
| Les articles L.1132-1 à -3 du Code du travail indiquent que :

	- « Aucune personne ne peut être écartée d'une procédure de recrutement ou de l'accès à un stage ou à une période de formation en entreprise, aucun salarié ne peut être sanctionné, licencié ou faire l'objet d'une mesure discriminatoire, directe ou indirecte, notamment en matière de rémunération, au sens de l'article L3221-3, de mesures d'intéressement ou de distribution d'actions, de formation, de reclassement, d'affectation, de qualification, de classification, de promotion professionnelle, de mutation ou de renouvellement de contrat en raison de son origine, de son sexe, de ses mœurs, de son orientation sexuelle, de son âge, de sa situation de famille ou de sa grossesse, de ses caractéristiques génétiques, de son appartenance ou de sa non-appartenance, vraie ou supposée, à une ethnie, une nation ou une race, de ses opinions politiques, de ses activités syndicales ou mutualistes, de ses convictions religieuses, de son apparence physique, de son nom de famille, de son lieu de résidence ou en raison de son état de santé ou de son handicap »,
	- « Aucun salarié ne peut être sanctionné, licencié ou faire l'objet d'une mesure discriminatoire mentionnée à l'Article L1132-1 en raison de l'exercice normal du droit de grève »,
	- « Aucun salarié ne peut être sanctionné, licencié ou faire l'objet d'une mesure discriminatoire pour avoir témoigné des agissements définis aux alinéas précédents ou pour les avoir relatés ».

| En conséquence, tout salarié de l’entreprise dont il est avéré qu’il s’est livré à de tels agissements sera susceptible de faire l’objet d’une des sanctions énumérées à l’article 15 du présent règlement intérieur.



Article 15 - Sanctions et droits des salariés
---------------------------------------------
| 15.1 Dispositions applicables aux personnels intérimaires, stagiaires et prestataires.
| A défaut de respecter les dispositions du présent règlement intérieur, les personnels concernés sont susceptibles de se voir retirer l’accès à l’entreprise.

| 15.2 Sanctions disciplinaires applicables aux salariés de RTE

| 15.2.1 Dispositions disciplinaires applicables aux salariés statutaires
| Tout comportement ou agissement d’un salarié statutaire considéré comme fautif par l’autorité compétente peut, en fonction de sa nature et de sa gravité, faire l’objet d’une sanction.

| 15.2.2 Dispositions disciplinaires applicables aux salariés non statutaires
| Tout comportement ou agissement d’un salarié non statutaire lié par un contrat de travail à l’entreprise, considéré comme fautif par l’autorité compétente, peut, en fonction de sa nature et de sa gravité, faire l’objet de l’une des sanctions suivantes sans suivre nécessairement l’ordre de ce classement :

	- avertissement,
	- blâme,
	- mise à pied, limitée à un mois, avec privation de salaire,
	- rétrogradation,
	- licenciement.

| 15.2.3 Dispositions communes applicables aux salariés statutaires et non statutaires
| L’employeur ne peut prononcer que les sanctions énoncées ci-dessus par le présent règlement intérieur. En outre, un fait fautif ne saurait correspondre automatiquement avec l’application d’une sanction donnée, l’appréciation des circonstances d’espèce relève du pouvoir de direction de l’employeur.
| Toute sanction est motivée par l’autorité compétente et dûment notifiée par écrit à la personne.

| 15.3 Sanctions pénales
| Des sanctions pénales peuvent être encourues en cas d’infraction et notamment pour des faits de harcèlement moral ou sexuel.
| Un tel délit est passible d’une peine de deux ans d’emprisonnement et de 30 000 euros d’amende. Selon les dispositions spécifiques de l’article 222-33 du Code pénal, le harcèlement sexuel est défini comme « le fait d’imposer à une personne, de façon répétée, des propos ou comportements à connotation sexuelle qui, soit portent atteinte à sa dignité en raison de leur caractère dégradant ou humiliant, soit créent à son encontre une situation intimidante, hostile ou offensante ».
| Est assimilé au harcèlement sexuel le fait, même non répété, d’user de toute forme de pression grave dans le but réel ou apparent d’obtenir un acte de nature sexuelle, que celui-ci soit recherché au profit de l’auteur des faits ou au profit d’un tiers.
| Ces peines sont portées à trois ans d’emprisonnement et 45000 euros d’amende lorsque les faits sont commis :

	- par une personne qui abuse de l’autorité que lui confèrent ses fonctions,
	- sur un mineur de moins de quinze ans,
	- sur une personne dont la particulière vulnérabilité due à son âge, à une maladie, à une infirmité, à une déficience physique ou psychique, à un état de grossesse ou résultant de la précarité de sa situation économique ou sociale - est apparente ou connue de leur auteur,
	- par plusieurs personnes agissant en qualité d’auteur ou de complice.

| Le Code du travail prévoit aussi une sanction pénale. Ainsi l’article L.1155-2 du Code du travail énonce que « les faits de harcèlement moral ou sexuel sont punis d’un emprisonnement d’un an et d’une peine d’amende de 3750 euros ».

| 15.4 Droits de la défense des salariés

| 15.4.1. Droits de la défense des salariés statutaires
| Les droits de la défense des salariés statutaires sont garantis par les dispositions légales et réglementaires.

| 15.4.2. Droits de la défense des salariés non statutaires
| Les droits de la défense des salariés non statutaires sont garantis par les dispositions légales en vigueur, à savoir


.. _rules_titre_iii:

TITRE III - DISPOSITIONS RELATIVES À L'HYGIÈNE, A LA SANTE ET A LA SECURITE
===========================================================================

| L’hygiène, la santé et la sécurité sont une priorité de l’entreprise qui doit en assurer l’effectivité.
| A ce titre, l’entreprise doit :

	- mettre en œuvre toutes les mesures de :
		- prévention appropriées pour assurer les conditions d’hygiène et de sécurité de nature à préserver la santé et l’intégrité physique et morale des personnels,
		- rétablissement des conditions de travail protectrices de l’hygiène, de la santé et de la sécurité des salariés dès lors qu’elles apparaissent compromises,
	- compléter et mettre à jour l’information du personnel sous sa responsabilité en matière d’hygiène, de santé et de sécurité applicable à l’accomplissement des travaux qu’il exécute,
	- contrôler le respect de ces consignes.

| Les mesures d’hygiène et de sécurité sont obligatoires pour tous les salariés statutaires ou non statutaires, stagiaires et intérimaires, et de façon générale, pour toute personne exécutant un travail dans l’entreprise, qu’elle soit liée ou non à celle-ci par un contrat de travail. Aussi les personnels doivent respecter et contribuer à faire respecter, en fonction de leurs responsabilités hiérarchiques, les consignes générales et particulières en vigueur sur les lieux de travail.
| Les personnels doivent veiller à leur sécurité personnelle, à celle de leurs collègues, du public et des installations.
| SUPAMONKS doit être particulièrement vigilant aux respects des règles mentionnées ci-après pour des raisons évidentes de protection des personnels, de qualité de vie au travail, de saine exécution des activités et de performance.


Article 16 - Respect des règles d’hygiène
-----------------------------------------
| Les personnels doivent respecter les règles élémentaires d’hygiène, d’ordre et de propreté, dans l’ensemble des locaux et lieux de travail, et tout particulièrement dans les vestiaires et les locaux sanitaires.
| Différents moyens de collecte des déchets sont mis à la disposition de chacun (conteneurs, poubelles, cendriers dans les espaces fumeurs et aux entrées). Le personnel doit s’astreindre à les utiliser en respectant les principes de tri sélectif des déchets mis en œuvre.
 

Article 17 - Vestiaires
-----------------------
| 17.1 Le personnel peut être amené à disposer d’armoires, de vestiaires mis à sa disposition et qui doivent être maintenus dans un état de propreté constante ; les salariés sont informés des opérations de nettoyage périodique.
| Conformément à l’article R4534-139 du Code du travail, « l'employeur met à la disposition des travailleurs un local vestiaire :

	- convenablement aéré et éclairé, et suffisamment chauffé,
	- nettoyé et tenu en état constant de propreté,
	- pourvu d'un nombre suffisant de sièges.

| Il est interdit d'y entreposer des produits ou matériels dangereux ou salissants ainsi que des matériaux. Lorsque l'exiguïté des lieux ne permet pas d'équiper le local d'armoires-vestiaires individuelles en nombre suffisant, le local est équipé de patères en nombre suffisant (…) ».

| 17.2 En cas de disparitions renouvelées et rapprochées d’objets et de matériels appartenant à l’entreprise ou à son personnel, ou en cas de problème d’hygiène, la Direction peut procéder à une vérification du contenu des armoires et des vestiaires, après avoir informé individuellement chaque salarié, obtenu leur consentement et en leur présence, lesquels peuvent demander la présence d’un tiers appartenant à l’entreprise ou un représentant du personnel. Cette vérification sera effectuée dans des conditions préservant la dignité et l’intimité des personnes concernées. En cas de refus, la direction pourra faire appel aux services de police compétents.
| L’ouverture de vestiaire non identifié et non revendiqué est possible, sous réserve qu’une information soit faite, trois semaines à l’avance, par affichage sur le dit vestiaire , de la date d’ouverture, étant précisé que cette disposition se limite aux seuls casiers non identifiés, dans le délai prévu et qu’elle ait lieu en présence d’un représentant du personnel.


Article 18 - Présence et consommation de boissons alcoolisées
-------------------------------------------------------------
| 18.1 Compte tenu des enjeux de sécurité inhérents aux activités réalisées en leur sein, l’introduction d’alcool est interdite au sein des postes électriques.

| 18.2 Conformément aux dispositions de l’article R4228-21 du Code du travail, il est interdit « de laisser entrer ou séjourner dans les lieux de travail des personnes en état d'ivresse ». Suite à constat d’ivresse manifeste, les personnes chargées des accès et les managers ont la responsabilité de prendre les mesures qui s’imposent (interdiction d’entrer, interdiction de séjourner, accompagnement de la personne au cabinet médical,…).
 

Article 19 - Contrôle de l’alcoolémie, dépistage de stupéfiants
---------------------------------------------------------------
| En raison de l’obligation faite à l’entreprise d’assurer la sécurité, la direction peut faire effectuer par une personne habilitée un contrôle de l’alcoolémie et de prise de stupéfiant sur les salariés affectés à    des postes de sécurité. Les postes de sécurité sont ceux nécessitant une habilitation pour travaux d’ordre électrique. Ces contrôles ne sont mis en œuvre que dans les postes de sécurité, dans les cas où l’état d’imprégnation alcoolique ou de stupéfiants est de nature à exposer les personnes ou les biens à un danger, de façon à prévenir ou à faire cesser immédiatement une situation dangereuse.


Article 20 - Interdiction de fumer
----------------------------------
| En application du décret 2006-1386 du 15/11/2006, les personnels sont tenus de se conformer à l’interdiction de fumer tant sur leur lieu individuel de travail que dans les lieux de travail à usage collectif, dès lors où ces derniers sont fermés et couverts, à l’exception des éventuels emplacements réservés aux fumeurs qui auraient été mis en place. Une signalisation rappelle cette interdiction.
| Les conditions de cette interdiction visent également la cigarette électronique et le vapotage.



Article 21 - Respect des règles de sécurité
-------------------------------------------
| La Direction assure la responsabilité de la sécurité au sein de l’entreprise.
| Il lui incombe à ce titre :

	- de mettre en œuvre et de faire assurer le respect de toutes les dispositions législatives et règlementaires qui s’imposent à elle en raison de son activité et de son organisation,
	- de diffuser et de mettre à jour l’information des personnels sous sa responsabilité.

| Les personnels sont informés des consignes de sécurité et d’incendie, du plan local d’évacuation d’urgence, qui sont affichés dans l’entreprise et doivent s’y conformer. Il en est de même pour les prescriptions spécifiques communiquées par notes de service, les directives et les consignes données par la hiérarchie.
| Les personnes en situation de visite sur un site seront également informées des dispositions en vigueur.
 
 
Article 22 - Protections collectives et individuelles
-----------------------------------------------------
| 22.1 Les personnels ont l’obligation d’utiliser, en toutes saisons, tous les dispositifs de protection installés sur les machines, les matériels et équipements. Outre l’entretien régulier réalisé par la direction, il appartient aux utilisateurs de signaler toutes éventuelles anomalies ou corrections nécessaires en la matière.

| 22.2 Pour tout risque qui ne peut être évité par une protection collective ou par l’organisation du travail, des Equipements de Protection Individuelle (EPI) sont mis à la disposition des personnels afin de prévenir tout accident. Les personnels sont tenus de porter ces EPI (selon le référentiel en vigueur), de les maintenir en bon état d’utilisation et de respecter les consignes relatives à leur utilisation et à leur entretien.


Article 24 - Installations et matériels électriques
---------------------------------------------------
| En vue d’informer, de former et d’assurer la sécurité des personnes contre les dangers d’origine électrique.

Article 25 - Formations
-----------------------
| Les personnels sont tenus de participer aux actions de formations obligatoires dans l’exercice de leur activité qui leurs sont destinées, sous peine de ne plus pouvoir exercer certaines activités inhérentes à leur emploi.

Article 26- Danger grave et imminent
------------------------------------
| En cas de situation de travail dont il a un motif raisonnable de penser qu’elle présente un danger grave et imminent pour sa vie ou sa santé, chaque membre des personnels est tenu de le signaler immédiatement à son supérieur hiérarchique (Article L.4131-1 du Code du travail).
| Aucune sanction ou retenue de salaire ne peut être prise à l’encontre d’un travailleur ou d’un groupe de travailleurs qui se retire d’une situation de travail dont il a un motif raisonnable de penser qu’elle présente un danger grave et imminent pour la vie ou pour la santé de chacun d’eux (Article L.4131-3 du Code du travail).
| La faculté offerte aux personnels de se retirer d’une telle situation de travail doit être exercée de telle manière qu’elle ne puisse créer pour autrui une nouvelle situation de risque grave et imminent. Il est précisé que tout exercice illégitime ou abus de droit est susceptible de faire l’objet d’une sanction disciplinaire.
 
 
Article 27- Respect des dispositions en situation de pandémie
-------------------------------------------------------------
| En cas d’activation d’un plan de continuité de l’activité, les salariés de RTE, dans le cadre de l’exercice de leurs fonctions, doivent se soumettre aux dispositions relatives à l’hygiène, à la sécurité et à la prévention des risques professionnels prévues par ce plan. Celui-ci est accessible sur l’Intranet de SUPAMONKS.


Article 28 - Respect des règles relatives à l’hygiène et la sécurité
--------------------------------------------------------------------
| Aux termes de l’article L4122-1, « il incombe à chaque travailleur de prendre soin, en fonction de sa formation et selon ses possibilités, de sa santé et de sa sécurité ainsi que de celles des autres personnes concernées par ses actes ou ses omissions au travail.
| Le salarié qui se rend compte que l’état d’un collègue est de nature à constituer une menace pour lui-même ou pour son entourage alerte la hiérarchie.
| Tout accident survenu au cours de travail (ou du trajet) doit être porté à la connaissance de la hiérarchie de l’intéressé dans les meilleurs délais.
| Conformément aux dispositions de l’article L441-1 du Code de la sécurité sociale, tout accident du travail doit être porté à la connaissance de l’employeur.


Article 29 - Médecine du travail
--------------------------------
| Tout salarié statutaire ou non statutaire doit se soumettre à une visite médicale de reprise auprès de la médecine du travail dans les conditions fixées par l’article R4624-22 du code du travail.
| En effet, le salarié bénéficie d'un examen de reprise du travail par le médecin du travail :

	- Après un congé de maternité ;
	- Après une absence pour cause de maladie professionnelle ;
	- Après une absence d'au moins trente jours pour cause d'accident du travail, de maladie ou d'accident non professionnel.
 

.. _rules_titre_iv:

Titre IV - ENTRÉE EN VIGUEUR ET MODIFICATIONS DU RÈGLEMENT INTÉRIEUR
====================================================================

Article 30 - Entrée en vigueur du règlement intérieur
-----------------------------------------------------
| Le règlement intérieur et d’ors et déjà en vigueur.


Article 31 - Modifications du règlement intérieur
-------------------------------------------------
| Toute modification ultérieure ou tout retrait de clause de ce règlement et ses annexes est, conformément au Code du Travail, soumis à la même procédure, étant entendu que toute clause du règlement qui deviendrait contraire aux dispositions légales, réglementaires ou conventionnelles applicables à l’entreprise du fait de l’évolution de ces dernières, sera considérée comme caduque.


Article 32- Affichage et communication
--------------------------------------
| Pour qu’il soit connu de tous, le règlement intérieur et ses annexes font l’objet :

	- d’une mise à disposition d’un exemplaire papier pour consultation dans le bureau de production,
	- d’une communication à chaque salarié du lien Intranet pour accéder aux documents,

| Pour les prestataires extérieurs, un exemplaire du règlement intérieur est joint aux marchés.