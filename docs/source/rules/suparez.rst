.. _suparez:
==========================
Charte de la Suparésidence
==========================

Introduction
------------
	Le principe de cette charte est d'établir sous quelles conditions Supamonks pourra accueillir des équipes souhaitant réaliser un projet personnel au sein du studio Supamonks dans le cadre de la “SUPARESIDENCE”.


Conditions préalables à l'accueil d'un projet externe au sein du studio Supamonks
---------------------------------------------------------------------------------

Station n°1 : un premier contact par voie numérique est nécessaire. Le porteur du projet enverra la description la plus complète de son projet. Supermoine aime tout savoir. Il n’est pas Dieu mais pas loin. Sur la base de cet envoi, divers réponses (argumentées) seront possibles de la part du concile de sélection Supamonks :

	- Dossier accepté 
	- Dossier rejeté mais représentable sur une autre session 
	- Dossier rejeté sans possibilité de nouvelle soumission

Station n°2 : dans le cadre d’un dossier numérique accepté, le porteur du projet sera reçu par des représentants du concile Supamonks pour une présentation de son projet. Le porteur du projet présente à cette occasion un planning de production et une requête de mise à disposition de postes de travail (combien de poste(s), combien de temps, etc.)

Station n°3 : par échange de mail, et éventuellement des présentations au sein du cloître, l’équipe de SUPARESIDENCE doit au préalable à sa venue valider une animatic 2D. Le concile se montrera aussi disponible et prodigue que possible pour permettre à l’équipe de la SUPARESIDENCE d’avancer aussi efficacement que possible sur cette étape.

Station n°4 : le porteur du projet se présentera avec son équipe pour commencer à date fixée la production de sa SUPARESIDENCE au cloître Supamonks. Il fera ses meilleurs efforts pour respecter le planning défini. 

 
Conditions de mise à disposition de l'infrastructure SupamonkS
--------------------------------------------------------------

- l'équipe de la SUPARESIDENCE doit respecter le matériel, les employés, les horaires de SupamonkS sous peine de se voir chatier à coup de chapelets
- l'équipe de la SUPARESIDENCE devra respecter le planning de production et d'occupation du matériel présenté par le porteur du projet. Toutefois, dans leur grande mansuétude, les directeurs(rices) de production du studio pourront examiner des demandes de modifications des plannings et d’occupation de postes (si elles sont raisonnables et justifiées). Générosité et bienveillance sont les mamelles dodues de la SUPARESIDENCE.
- l'équipe de la SUPARESIDENCE, via son représentant, devra systématiquement tenir informé Supamonks d'absence inopinées.
- l'équipe de la SUPARESIDENCE devra systématiquement présenter, préalablement à son arrivée dans les locaux, un éventuel nouveau membre.
- en cas de non respect des plannings pour "cas de force majeur", le représentant de la SUPARESIDENCE devra faire "au meilleur de ses moyens" pour tenir informer l'équipe de SupamonkS.
- l'équipe de la SUPARESIDENCE devra respecter les horaires d’ouverture du cloître (10h-19h). Toutefois, tant qu’il restera le soir un moine détenteur des clés au cloître, l’équipe de la SUPARESIDENCE pourra continuer à travailler. Supermoine met en garde “plus la lumière de la bougie est forte plus vite elle se consume”. On imagine qu’il veut signifier par là qu’il faut savoir gérer son effort et s’accorder des heures de sommeil et des jours de relâche.
- l'équipe de la SUPARESIDENCE pourra éventuellement recevoir une clé du cloître pour travailler en dehors des horaires ou des jours d’ouverture. Cela ne pourra arriver que dans des cas exceptionnels. Sera remis au responsable de la SUPARESIDENCE le sésame d’accès au studio (que d’aucuns appellent “code de l’alarme”).
- il est entendu qu’aucune notion de rémunération ne pourra avoir de place au sein d’une SUPARESIDENCE. La SUPARESIDENCE fonctionne sur le bénévolat de toutes les parties. Supermoine y est très vigileant.

Mise à disposition de l'infrastructure et des ressources humaines SupamonkS
---------------------------------------------------------------------------

Supamonks met à disposition de la Suparésidence :

- Ses locaux (espace de travail et lieux de vie commun)
- 4 à 5 postes de travail équipés de licences Maya/V-Ray/Nuke/AfterFX/Photoshop (ou Krita) - Si toutefois l’équipe de SUAPRESIDENCE avait bsoin d’autre licences cela devrait donner lieu à une discussion préalable avec le concile pour juger de la faisabilité
- Son pipeline (workflow de travail) dans lequel devra travailler la SUPARESIDENCE (une formation sera prodiguée à l’équipe)
- Un chargé de production qui veillera au respect des plannings par l’équipe
- L’équipe de supervision qui pourra apporter tous les conseils utiles mais qui ne peut en aucun cas devenir un membre de l’équipe de la SUPARESIDENCE. Les superviseurs participeront aux Milestones hebdomadaires et partageront leur expérience avec l’équipe de la SUPARESIDENCE.
- Si l’équipe de SUPARESIDENCE souhaitaient réaliser un crowdfunding pour accompagner son projet, Supamonks peut proposer une mise en relation via une relation privilégiée avec Ullule.

.. important::
	Supermoine est généreux il ne demande rien en contrepartie de toutes les aides susmentionnées à l’exception :
		- Du respect absolu de cette charte
		- De la présence au générique de fin de la mention “réalisé dans le cadre de la SUPARESIDENCE” accompagné du logo Supamonks Studio





