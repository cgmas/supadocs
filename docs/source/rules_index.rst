.. _rules:

=================
Les SupaChartes !
=================

A Supa il y a quelques chartes et règles, les voici :

.. toctree::
   :maxdepth: 1
   :caption: Sommaire:

   La SupaCharte de Vie <rules/way_of_life>
   La SupaCharte de la SupaResidence <rules/suparez>
   La SupaCharte de l'écolovie <rules/eco_rules>
   Le règlement intérieur <rules/life_rules>   