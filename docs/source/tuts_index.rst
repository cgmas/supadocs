.. _tuts:

============
Les SupaTuts
============

Les SupAdmins
=============

.. toctree::
   :maxdepth: 1

   L'intermittent du spectacle <tuts/intermittence>
   Les livraisons Quick et BK <tuts/quick_bk_delivery>

Les supabases
=============

.. toctree::
   :maxdepth: 1

   Gestion du poste de travail <tuts/workstation>
   Kabaret <tuts/kabaret>
   VRAK by Claire Besson <tuts/vrak>
   VRAK <tuts/vrak_max>

.. toctree::
   :hidden:

   ZBrush <tuts/maya>
   Maya <tuts/maya>
   Nuke <tuts/nuke>
   AfterEffect <tuts/after_effects>
   Krita <tuts/krita>
   Photoshop <tuts/toshop>
   Resolve <tuts/resolve>
   Muster <tuts/muster>

.. Les supaflow
.. ============

.. toctree::
   :maxdepth: 1
   :hidden:

   Le flow <tuts/wf>
   Le modeling <tuts/modeling_wf>
   Le rigging <tuts/rigging_wf>
   Le surfacing <tuts/surfacing_wf>
   Le hair <tuts/hair_wf>
   L'anim <tuts/anim_wf>
   Le CFX <tuts/cfx_wf>
   Le VFX <tuts/VFX>
   Le lighting <tuts/lighting_wf>
   Le rendu <tuts/render_wf>
   Le compo <tuts/comp_wf>
   Le polish <tuts/polish_wf>
   Le montage <tuts/edit_wf>

Les supaprojects
================

BDS
---
.. toctree::
   :maxdepth: 1

   VRAK <tuts/vrak_bds>

QUICK
-----

todo