.. _modeling:

#################
LE MODELING
#################

Les outils
==========

Les outils a disposition sont les suivants:

	- ZBrush
	- Maya
	- Blender
	- UVLayout

Le flow
=======

| Pour la plupart des modélisation organique on utilise ZBrush.
| Le soft n'est pas disponible dans Kabaret. Il est installé sur la machine.

| L'idée sera de travailler