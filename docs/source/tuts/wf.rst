.. _supaflow:

#################
LE FLOW
#################
| Notre manière de faire !

| Notre workflow utilise:

	- Photoshop ou Krita
	- ZBrush
	- Maya
	- Substance Painter
	- Mari
	- Yeti
	- VRay
	- Vrak
	- VSL
	- Nuke
	- After Effect
	- Resolve

| En fonction des projets le découpage des taches n'est pas forcément répartis de la même manière.
| Généralement un projet avance selon le flow suivant :

=======================
PRE PROD
=======================