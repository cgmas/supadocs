.. _muster:

#################
MUSTER
#################
| C'est le dispatcher utilisé à Supamonks.
| Il permet de lancer et gérer des rendus Maya/Vray et Nuke sur la renderfarm.