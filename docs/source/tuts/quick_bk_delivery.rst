.. _quick_bk_delivery:

********************************************************
Les livraisons Quick et BK pour la France et la Belgique
********************************************************
Logiciels requis :
==================
	- Adobe After effect
	- Resolve
	- Internet (un compte sur Imd)

.. _master_uncompressed:

1/ Master Uncompressed
======================
Après validation de Quick/BK et de la licence Française et Belge, après réception des cartons pour l’ARPP, et des mix defs antennes ; lancez les masters sur After effect.

Vérifiez bien :
	- Les mentions
	- Les copyrights
	- Les mixs defs
	- Le timing

Il n’y a plus besoin de faire les 50 frames de noirs etc… il faut lancer les montages de la frame 1 jusqu’à la dernière image de la signat’
	- Mention L
	- Mention M
	- Mention N
	- Mention O
	- Belgique en FR
	- Belgique en NL

En Quicktime/Uncompressed avec cette nomenclature : 
	| QXX_FR_L[1920x1080_23s_Uncompressed]
		| Q ou BK
		| L ou M, N ou O
		| 23s ou 18s

1. Ouvrez Resolve
2. Glissez les masters dans la partie media, faire les rendus un par un dans la timeline
3. Dans les paramètres de rendu Vidéo
4. Format : MXF OP1A
5. Codec : XDCAM MPEG2
6. Cochez Field rendering
7. Frame rate 25
8. Dans les paramètres de rendu Audio
9. Codec : Linear PCM
10 .Channels : 2
11. Bit Depth : 24
12. Tracks : One per channel
13. Dans les paramètres de rendu File
14. Utilisez la nomenclatures des masters QXX_FR_M[1920x1080_23s_MXF]
15. Les sortir **en local** et les ranger ensuite dans le dossier master sur le réseau.

2/ Livraison Française
======================
Connectez-vous sur votre compte http://www.groupimd.com/

	.. image :: /images/QBK_delivery/imdcloud.jpg

Créer une nouvelle commande pour la France :
	1. Référence interne : QXX_FR_V01
	2. Agence : The Brand Nation
	3. Annonceur : Quick
	4. Marque/Produit : Mettre à la lettre près l’intitulé sur le carton pour l’ARPP reçu
	5. Sauvez

| Entrez dans la commande, indiquez pour les 4 fichiers, le numéro de pubID indiqué sur le carton fourni par TBN, ainsi que le titre et la version d’agence.
| Indiquez pour chaque fichier la mention correspondante, le Format (HD) et uploadez le fichier .mxf

| Attendez le mail de confirmation du contrôle qualité réussi pour les 4 fichiers (sans quitter la page) puis dans la partie « destinations » en dessous, tapez « ARPP » dans la case « Chaines », puis pour chaque fichier sélectionnez une livraison « Std » dans le menu déroulant correspondant. Et cliquez en bas sur « Envoyer »

	.. image :: /images/QBK_delivery/imdref.jpg

| Tout en haut du site d’imd, il y a Bibliothèque/Commandes/Commandes visio/suivi – Cliquez sur « Bibliothèque » puis Livraison, puis sur le dossier de votre livraison.
| Pour chaque vidéo cliquez sur les 3 barres en haut à droite des vignettes, et cliquez sur « E-mail vidéo »

	.. image :: /images/QBK_delivery/imdsupa.jpg

Tapez votre mail, sélectionnez le format ARPP, cochez « Autoriser le téléchargement » et mettez dans « Message » le nom de la mention du fichier (L/M/N ou O). Cliquez sur « Envoyer » et faîtes de même pour les 3 autres fichiers.

Vous allez recevoir 4 mails, cliquez sur « view vidéo » et en bas de la fenêtre qui s’ouvrira, téléchargez les fichiers pour les 4 mails.

Créez un dossier « ARPP » dans le dossier master sur le réseau, et rangez vos fichiers .mpg4 faire une archive .zip en la nommant « QXX_MPEG_ARPP.zip» et envoyez là à TBN pour qu’ils soumettent à l’ARPP.

Si TBN vous dit que c’est validé ARPP passez à la suite, si ce n’est pas le cas, revenez à :ref:`Master Uncompressed <master_uncompressed>`

2/ a) Livraison aux régies
--------------------------
| TBN doit vous avoir envoyé des specs de livraison pour la TV France, le WEB France, la TV Belges, et le Web Belge.
| Il est très important, de les ranger au bon endroit, dans QUICK/PROD/RECEPTION/QXX_LICENCE

| Retournez sur la commande sur le site http://www.groupimd.com/ 
| Retrouvez l’endroit où vous avez livré « ARPP » et livrez à toutes les régies demandées sur les specs.

.. attention::
	Pour le WEB, choisir « Quick Facebook » si Facebook est à livrer, et tous les « via MEC Global » (Gulli, MyTF1 et Pluzz) sinon on se fait engueuler.

Cliquez sur Envoyez.

2/ b) Livraison Youtube
-----------------------
| Ouvrez After effect et lancez des H264 avec le son de Gilles pour « youtube » (à ranger dans dossier master) :
| En 1920x1080 de la mention L
| En 1920x1080 de la mention L avec bandes blanches
| Les envoyer à TBN.

3/ Livraison Belge
==================
EXEMPLE nomenclature : QUICK – Kids – Wave 5 – Playmobil Hasbro

Vous allez recevoir ou avez reçu, un mail de la part de Quick Belgique concernant une nomenclature à adopter pour le spot, en plus des specs de livraison.

Connectez-vous sur votre compte http://www.groupimd.com/

Créer une nouvelle commande :
	1. Référence interne : QXX_BELGIQUE_V01
	2. Agence : The Brand Nation
	3. Annonceur : Quick
	4. Marque/Produit : Mettre le produit indiqué dans les spec de livraison Web Belge
	5. Sauvez

Choisir un template de Belgique FR ou NL comme ci-dessous (tout à droite)

	.. image :: /images/QBK_delivery/imdtemplate.jpg

	| Mettre dans PubID : QXX_BE ou NL en fonction de la langue
	| Titre : Mettre la nomenclature établie par l’agence Quick Belge
	| Durée : 20
	| Langue : Renseignez la langue
	| Mentions : 00 pas de mentions
	| Format : HD
	| Et uploadez les MXF de la Belgique
	| Sauvez

3/ a) Livraison aux régies
--------------------------
Une fois les 2 remplis, uplaudé et le contrôle de qualité réussi, vous pouvez livrer les chaînes et les régies indiquées sur les specs.

.. attention ::
	Aux langues, certaines chaînes a besoin des deux, d’autres seulement une etc…

Pour le web choisir Cadreon HD Belgium et YOKI Belgium (sauf contre-indication dans les specs)

Envoyez.

3/ b) Livraison Youtube
-----------------------
Ouvrez After Effect, faire des H264 en 1920x1080 avec les mixs de Gilles « youtube » BE et NL, les ranger dans master sur le réseau et les envoyer à TBN.

Une fois les régies livrées, vous allez recevoir un mail de confirmation de livraison aux chaînes, transférez le à TBN.


Vous avez terminé la livraison ! Bravo !

 



