.. _intermittence:

*************************
INTERMITTENT DU SPECTACLE
*************************

Tu viens d’être embauché(e) en tant qu’intermittent du spectacle à Supamonks et c’est ta première fois ? Ce tuto est fait pour toi !

DISCLAIMERS
===========
| Ce document a été crée en Septembre 2018, il est donc susceptible d’être obsolète, certaines données seront inexactes, et mériteraient d’être vérifiées régulièrement.
| Le statut est en continuelle évolution, tiens toi informés sur les sites officiels.

	https://www.intermittent-spectacle.fr/-actualites-.html

EN BREF
=======
L’intermittence du spectacle, est un régime de cotisation sociale, qui permet, après une justification de cotisation de 507h à ce régime en 12 mois, de bénéficier de la part de pôle emploi d’une indemnisation de 75% (environ) de son salaire lorsqu’on ne travaille pas.

Les droits des intermittents changent quasiment tous les ans, n’hésite pas à te tenir informé régulièrement.

PREMIER PAS
===========
| Tu vas recevoir par mail, un lien vers un questionnaire administratif à remplir avant d’arriver au studio qui va permettre à Supamonks de rédiger ton premier contrat chez nous.
| Dans ce même mail tu auras la période estimative de ta mission chez nous (les dates), sur quel projet tu es prévu, ton tarif € brut par jour et le nombre d’heures quotidiennes (en général 8h chez nous).
| Toutes ces informations sont très importantes, conserve-les précieusement.

| Lors de ton premier jour chez nous, tu devras signer un contrat. Tu devras y vérifier les informations administratives et noter s’il y a des erreurs. Seront indiqués: ton tarif €/j, tes horaires,  la durée de ta mission (estimative) etc… 
| Lorsque ton contrat est établi, pour anticiper des annulations opportunes de production, nous garantissons donc une durée de jours payés minimale dans le mois (souvent la moitié de ton temps prévu dans le mois). Par exemple, même si la production se terminait à ton deuxième jour de travail, si ton contrat t’assures 5 jours chez nous, tu seras payé 5 jours. Mais pas de panique, à la fin du mois, un avenant à ton contrat te sera desservi avec une correction (forcément supérieure) du nombre de jours exact que tu auras travaillés chez nous.


A LA FIN DE CHAQUE MOIS
=======================
| Chaque fin de mois est une fin de contrat pour tous les intermittents du spectacle (sauf pour les contrats longs) tu vas donc recevoir :
	- Un avenant à ton contrat (avec la durée réelle de présence de ta mission)
	- Une fiche de paie
	- Une AEM (attestation emploi mensuelle)
	- Une fiche de congés spectacle

CONGÉS SPECTACLE
================
| Comme tous les salariés, un intermittent du spectacle a le droit à 2,5 jours de congés payés par mois, pour un mois à temps complet travaillé.
| Tous les mois, la fiche des congés spectacle récapitule le nombre de jours de ces congés spectacle acquis.

Les congés spectacle viennent d’une caisse spéciale, gérée par un organisme appelé “Audiens”. Afin de traquer automatiquement les jours de congés acquis, il faut fournir un numéro d’immatriculation au congés spectacle à ton employeur.

| Pour l’intermittence, les jours de congés sont sous forme d’argent fourni par cette caisse. Chaque demande de congés spectacle se fait annuellement, à partir du 1er Mai de chaque année. La période comptabilisée est de Mai au 30 Avril de chaque année.

Comment obtenir une immatriculation ?
-------------------------------------
Il suffit de remplir le formulaire en ligne `ICI <https://www.conges-spectacles.com/immatweb/saisieFormulaireAction.action>`_, et au bout de quelques semaines tu recevras ton numéro d’immatriculation qui sera à renseigner à tous tes employeurs en début de mission.

Quand faire un numéro d’immatriculation ?
-----------------------------------------
| Dès que tu as été embauché(e) ou prévu(e) d’être embauché(e) en tant qu’intermittent du spectacle, tu peux faire une demande de numéro d’immatriculation congés spectacle.
| Une fois le numéro d'immatriculation reçu, tu vas pouvoir avoir un compte personnel sur le site https://conges-spectacles.audiens.org/home.html

| J’ai eu mon numéro d’immatriculation après avoir eu ma première fiche de paie, mes jours de congés acquis sont-ils comptabilisés quand même ?
| Ils sont en effet comptabilisés, mais ces jours n’ont pas été traqués automatiquement par Audiens, il faudra donc certainement lors de ta demande de congés annuelle, envoyer ta justification de ces jours de congés (donnée à chaque fin de mois).

Comment obtenir mes jours de congés ?
-------------------------------------
| Tous les ans en Avril, tu vas recevoir un questionnaire de la part d’Audiens à remplir, de demande de prise de congé. Il faudra regarder sur ton compte congés spectacle si l’intégralité de tes période de travail ont bien été comptabilisée.
| Tu vas renseigner à partir de quelle date tu souhaites prendre tes congés (c’est-à-dire recevoir les sous correspondants à ces périodes de congés). En général tous les intermittents indiquent la première ou deuxième semaine de Mai afin d’être payés au plus vite. Tu recevras le paiement au plus tôt 15 jours avant la date indiquée sur ton questionnaire.

| Certains de mes jours de congés spectacle ne sont pas présents sur mon compte “congés spectacle”, que faire ?
| Tu peux envoyer tes fiches de congés spectacle des mois travaillés qui n’ont pas été comptabilisés directement à la caisse Audiens pour régulariser ta situation.


AEM
===
Une AEM est une attestation employeur mensuelle, c’est un justificatif de cotisation au régime de l’intermittence du spectacle qui a de la valeur auprès de Pôle Emploi. A conserver précieusement.

PÔLE EMPLOI
===========
Le régime de l’intermittence est étroitement lié à Pôle Emploi, étant donné qu’un intermittent du spectacle cotise pour lui-même, lorsque qu’il active ses droits à l’intermittence, pôle emploi lui verse environ 75% de son salaire pour chaque jour non travaillé.

Je ne suis pas inscrit à pôle emploi
------------------------------------
| Lorsque tu auras cumulé un total de 507 heures de justification de cotisation au régime de l’intermittence tu auras le droit à ton statut, et donc aux allocations de retour à l’emploi ARE.
| Afin de les débloquer auprès de pôle emploi, tu devras t’inscrire en ligne en tant que demandeur d’emploi (même si tu travailles encore) en indiquant comme dernière date de rupture de contrat le dernier jour travaillé du mois dernier en indiquant comme motif “fin de CDD(U)”.
| A la fin de ton inscription tu auras le choix entre 2 voire 3 dates pour un entretien auprès d’un des conseillers du pôle emploi le plus proche de l’adresse que tu auras indiquée.

Je prépare mon rendez-vous Pôle Emploi
--------------------------------------
| Pôle emploi est connu pour sa lenteur et son incompétence administrative, voici donc quelques conseils pour bien préparer ton entretien ci-dessous.
| Je t’invite à venir muni de ces documents :
	- Toutes les AEM qui justifient tes 507 heures (normalement ils les ont déjà, mais nous ne sommes pas à l’abri d’une mauvaise surprise)
	- Ta carte d’identité, et une photocopie recto-verso
	- Un RIB (original, qui vient de ton carnet de chèque)
	- Ta carte vitale, et une photocopie recto-verso
	- Ton CV

Il vont te demander si tu es interessé par des ateliers comme “comment bien préparer un entretien d’embauche ?” “comment bien rédiger un CV ?” je te suggère de les décliner…

Tu ne devras surtout pas sortir de ton entretien sans avoir :
Ton identifiant et mot de passe de ton compte pôle emploi
Avoir rempli (en entier) ta demande d’ARE
C’est le plus important, c’est le formulaire qui te donnera le droit à ton statut.
Ma demande d’ARE a été acceptée, et après ?
Tu vas avoir une période de carence de minimum 1 mois, avant de toucher à tes allocations.Cette période est plus grande plus ton tarif (lors de tes 507h) est plus élevé. Mais cette période est rétro-active.
Lors de l’acceptation de ta demande d’ARE, ce jour même est défini comme ton jour “anniversaire” de droit. A partir de ce jour, tu as 12 mois pour refaire tes 507h et débloquer à nouveau tes droits pour l’année d’après.

Actualisation
-------------
| Toutes les fins de mois, tu devras te connecter sur ton compte personnel pôle emploi afin de déclarer tes heures travaillées dans le mois et chez quels employeurs elles ont été faites. Tu auras une période d’environ 15 jours pour réaliser cette actualisation.
| A chaque fin d’actualisation, tu dois impérativement cocher la case “je suis toujours à la recherche d’un emploi” si tu es toujours intermittent, car l’intermittent est en permanence au chômage pour Pôle emploi (il a d’ailleurs sa carte demandeur d’emploi et peut bénéficier de tarifs préférentiels)

| Attention, c’est à ce moment-là que tout doit être raccord, les employeurs vont de leur côté déclarer chaque salarié intermittent, leurs nombre de jours, les dates précises, leurs nombre d’heures et leurs tarifs. Il faut que cette déclaration soit raccord avec la déclaration personnelle de l’intermittent sinon Pôle emploi bloque les allocations - et c’est la merde.
| Il est conseillé de bien demander à son employeur avant de se déclarer : les dates des jours, le nombre d’heures déclarées par jour etc… si on a un doute ou que ces informations ne figurent pas sur l’avenant au contrat afin d’éviter tout souci.

Calcul de mon allocation mensuelle
----------------------------------
Pôle emploi calcule le nombre de jours d’indemnisation comme ceci :

	(Nb d’heure travaillées x 1,4) / 8 = Nombre de jours non indemnisés dans le mois.

Exemple : Au mois de Septembre j’ai travaillé 15 jours ouvrés de 8h/ jour

	( 15 x 8 x 1,4) / 8 = 21  (nda : pôle emploi arrondit toujours à l’inférieur) 

Pour pôle emploi, j’ai donc travaillé 21 jours dans un mois de 30 jours, j’ai donc droit à 30 - 21 = 9 jours d’allocations 


JOURS FRANCHISES
================
Depuis la réforme de Juillet 2016, les intermittents ont des jours franchisés à écouler pendant l’année de leurs droits. Ils sont de 2 jours par mois. Ce sont des jours de “carence” et ne sont donc pas soumis à l’allocation.
Si ces deux jours ne sont pas pris dans le mois, ils sont reportés au mois suivant, ce qui donne 4 jours dans le mois suivant etc…
Ces jours franchisés doivent ensuite tous être pris avant la date anniversaire de l’ouverture des droits.

SÉCURITÉ SOCIALE
================
La CPAM est la sécurité sociale des salariés : un intermittent du spectacle est un salarié qui cotise pour un régime de cotisation différent, mais il reste un salarié. Il peut donc cotiser à la CPAM la plus proche de son adresse.

Je suis encore déclaré(e) en tant qu’étudiant, mais je ne le suis plus, que faire ?
-----------------------------------------------------------------------------------
| Il suffit de remplir le `formulaire de changement de situation <https://www.ameli.fr/sites/default/files/formualires/172/750.cnamts.pdf>`_ sur le site ameli.fr et de l’envoyer à votre caisse d’Assurance Maladie accompagné de l’avis d’admission à Pôle emploi, un RIB et une copie de votre carte d’identité.

Il est fortement conseillé, si ce n’est pas déjà le cas, de se faire un compte ameli.fr dans les plus bref délais (les procédures sont très longues !)

MUTUELLE (ASSURANCE SANTE)
==========================
| Une mutuelle (assurance santé) est obligatoire pour tous les salariés au régime général, et elle est prise à la hauteur de 50%, à la charge de l’employeur.
| En tant qu’intermittent, tu peux souscrire à la mutuelle de l’entreprise, mais ton employeur n’étant pas unique, tu ne bénéficieras donc pas de la prise en charge des 50%. Un intermittent n’est pas obligé de souscrire à la mutuelle de l’entreprise, et il n’est d’ailleurs pas obligé de souscrire à une mutuelle (assurance santé) du tout

CUMUL DES RÉGIMES
=================
J’ai déjà un statut de freelance, puis-je être déclaré(e) en tant qu’intermittent ?
-----------------------------------------------------------------------------------
Il est tout à fait possible de cumuler les deux statuts.

.. attention::
	Il est interdit légalement de déclarer plus de 48h de travail par semaine par le Code du Travail.

Après 6 semaines de travail à 48h par semaine d’affilées, il est obligatoire d’avoir une semaine d’inactivité.

J’ai mon statut d’intermittent et on me propose un CDD au régime général, est-ce possible de cumuler les deux ?
---------------------------------------------------------------------------------------------------------------
Bien sûr, en revanche, pendant la période de ton CDD tu ne cotiseras pas pour ton statut d’intermittent afin de renouveler tes droits. Tu devras néanmoins continuer de t’actualiser tous les mois en cochant la case “toujours à la recherche d’un emploi”.
Attention : Il est interdit légalement de déclarer plus de 48h de travail par semaine par le Code du Travail.
Après 6 semaines de travail à 48h par semaine d’affilées, il est obligatoire d’avoir une semaine d’inactivité.

FAQ
===
J’ai oublié de cocher la case “je suis toujours à la recherche d’un emploi” à la fin de mon actualisation, que va-t-il se passer ?
----------------------------------------------------------------------------------------------------------------------------------
	| Pôle emploi va donc te désinscrire de la liste des demandeurs d’emploi et stopper tes allocations.
	| Pour reprendre ton compte et tes allocations, il faudra refaire toutes les démarches depuis l’inscription. (Bonne chance !)

On m’a proposé un rdv pôle emploi d’inscription qui n’est pas un pôle emploi spectacle, dois-je en faire un autre ensuite ?
---------------------------------------------------------------------------------------------------------------------------
	| Tous les pôles emploi de France gèrent les intermittents du spectacle. Tu n’auras donc pas de double rdv à faire, ton dossier sera ensuite transmis à Nanterre, qui gère les demandes d’ARE.

J’arrive à la fin de mes droits à ma date d’anniversaire, quelles démarches dois-je faire ?
-------------------------------------------------------------------------------------------
	| Si tu as fait tes 507h, aucune, ton dossier sera traité, pour vérifier tes jours franchisés, et ré-ouvrir tes droits pour l’année d’après.
