.. _workstation:

=======================================
Gestion du poste de travail à Supamonks
=======================================

Le présent document regroupe les informations nécessaires à la bonne utilisation des postes de travail du studio.

Session de travail
------------------

| Pour travailler vous devez être connecté à une session. 
| Pour se connecter, vous disposez d’un identifiant et d’un mot de passe.

| L’identifiant par défaut suivra le schéma suivant:
    -pour Jean Dupont : “jdupont”

| Si un pseudo ou un alias vous a été demandé lors d’un entretien, il pourra aussi être utilisé comme identifiant.
| Si ni votre pseudo ni votre identifiant par défaut ne semble fonctionner, veuillez contacter le chargé de production de votre projet.

| Le mot de passe est le même pour tout le monde : “supamonk09,”

Environnement de travail
------------------------

| Quand vous êtes sur votre session, le bureau windows, les dossiers documents, téléchargement etc… sont les votre. 
| En changeant de poste de travail vous conservez votre environnement de travail.
| Cet environnement se trouve sur le disque E:/

| Les informations contenues dans votre profil, les dossiers de téléchargement, documents, images de windows correspondent à votre environnement de travail personnel.
| Ces données vous suivront d’un poste de travail à l’autre.
| Si le volume de donnée est trop important, l’ouverture de session windows peut être long car à chaque ouverture le système s’assure que les données suivent et sont les bonnes.
| Ceci pour avoir la possibilité de garder son environnement de travail d’un poste à l’autre.

Chaque ordinateur à 3 partitions de disque dur en local : 
    - C:/ -> pour windows et les programmes par défaut
    - D:/ -> pour les programmes pros
    - E:/ -> pour l’environnement de travail et les fichiers temporaires.

| Vous avez donc la possibilité de créer un ou plusieurs dossiers sur le disque E:/.
| Il faut garder en tête que ce disque sera régulièrement vidé.

| Si un disque dur est plein, prévenez Zwib, Max, ou Saf.
| Attention certains logiciel pro, comme ceux de la suite Adobe, utilisent par défaut énormément d’espace disque sur C:/.
| Si le disque C:/ est plein ou se remplit rapidement regardez dans les préférences de l’application si les stockages par défauts ne se font pas sur C:/ plutôt que sur le disque E:/ comme ils le devraient.

Il est important de garder en tête que les informations contenues sur les disques dur de l’ordinateur ne sont pas sauvegardées ni partagées.

Les données importantes relatives au projet doivent être stockées sur le réseau.

Le réseau
---------

| Plusieurs disques durs sont accessibles depuis votre ordinateur.
| Ce sont les disques durs de travail.
| Ces disques durs sont des disques du réseau.

Ils sont gérés et sauvegardés régulièrement.

Vous ne devez en aucun cas altérer les données sans en avoir référé à vos supérieurs ou responsables de projet.

Leur utilisation est répartie de la manière suivante :
    - IT - I:/ -> pour les programmes installés sur le réseau
    - SUPALIB - L:/ -> pour l’archivage des banques de géométries, textures etc… 
    - PROJECTS - P:/ -> pour toutes les données relatives aux projets
    - TDS - T:/ -> pour l’environnement technique
    - MINUS - M:/ -> pour le projet Minuscule



