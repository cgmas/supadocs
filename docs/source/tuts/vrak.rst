.. _vrak:

#################
LES BASES DE VRAK
#################
by Claire Besson

************
I- INTERFACE
************
a) Qu’est ce que c’est Vrak ?
=============================

Vrak est une fenêtre qui regroupe les shader, parfois les lights, les render settings, et les render layers d’une scène.
Le Vrak ouvert peut appeler d’autres Vraks en référence. 
Dans une scène de lighting, on aura en référence :

	- les Vraks globaux du film (Default lighting, Default Shading, Render Settings)
	- les Vraks des assets présents dans le casting de la scène.

En cas de modification du Vrak de la scène et des autres Vrak, il faudra penser à enregistrer TOUS les Vraks ayant reçu des modifications !

b) Ouvrir Vrak
==============

* Pour ouvrir l’interface de VRAK, on va tout à gauche de maya, dans Kabaret Studio, cliquer sur la flèche à droite de Vrak Tool > Open Vrak

.. image :: /images/vrak/v_open.jpg

* Si on double clique sur le root, on voit qu’il y a plusieurs onglets. De base on a ASSETS, LIGHTING, RENDER et SETTINGS

.. image :: /images/vrak/v_layout.jpg

La colonne de gauche, c’est “l’espace de travail”.
La colonne de droite, ce sont les “opinions”, les modifications créées, qu’il faudra enregistrer.


c) Les Onglets du Root: 
=======================

* Dans **ASSETS** il y a :
	- le 1All (ou AboveAll) dans lequel se trouvent les actions qui affectent tous les assets : le smooth de la scène, un shader gris de base, etc.)
	- Les shadings, rangés selon le type d’asset : CHAR, PROP et SET
* Dans **LIGHTING** il y a les différents sets de light du projet
* Dans **RENDER** il y a les différents renders layers :
	- Progressive: 
	 	permet un calcul rapide mais avec une qualité d’image assez basse de rendus
	- RGB:
		calcule en bucket, est plus long mais donne une image plus belle. 
	- Render Mask: 
		il rend juste un alpha et sert à vérifier les selectors
	- c’est ici qu’on peut créer de nouveaux render layers (si on souhaite séparer le perso du décor, ou autre).

* Dans **SETTINGS** il y a : 
	- les ELEMENTS, les passes rendues. Les passes RGB (spec, diffuse, relfection, etc.) et les XPASS (les extra passes : Point Camera, Point World, ID, etc.). Ils correspondent au Render Elements de Maya (dans les Render Settings)
	- les SETTINGS, ils ont souvent le nom du renderer dans lequel on utilise ces settings. On y trouve mêmes options que dans le Render Settings de Maya.  

d) Les petits truc utiles à savoir dans VRAK:
=============================================

Le CTRL + Z n'existe pas !

Supprimer:
----------
Quand on veut supprimer un paramètre qu’on a créé. Il faut sélectionner ses opinions dans la fenêtre de droite, et faire clic droit → delete. Refresh.
On peut aussi faire clique droit sur le paramètre dans la fenêtre de gauche > delete


Désactiver un paramètre/shader/etc. : 
-------------------------------------
clique droit sur le paramètre > desactivate
Tout ce qui est désactivé s’affiche en rouge et italique.

Le Rafraîchissement:
--------------------
Pour mettre à jour votre vrak après certaines actions (suppression d’un dossier, enregistrement d’un vrak, etc.) il existe plusieurs possibilités : 
-décocher et recocher le bouton Scene en haut à gauche du Vrak

.. image :: /images/vrak/v_scenes_button.jpg

-clique droit refresh dans la fenêtre de droite
-clique droit refresh dans la fenêtre de gauche (Attention, il vous ramène sur le Root du Vrak et ferme vos onglets “ASSETS”, “RENDER”, etc.)

Stage:
------
Le vrak d’une scène est aussi accessible depuis Kabaret dans l’onglet lighting et s’appelle “Stage”

e) Sauver son Travail:
======================

* Pour sauver le Vrak de la scène sur laquelle on travaille actuellement, il faut faire: clic droit dans l’espace de gauche et save. 
	Si on reboote VRAK (clique sur la flèche à droite de Vrak Tool > Reboot VRAK), on aura toujours notre travail en cours. 
	Le reboot de VRAK peut être utile. En effet, si on s’est trompé ou qu’on n’est pas content de son travail, plutôt que de s’embêter à supprimer ou déplacer certains fichiers dans VRAK, on peut très bien rebooter notre VRAK. On reviendra alors sur le dernier VRAK enregistré. 
	Cela permet aussi de mettre à jour les Vraks du film qui sont en référence, s’ils ont été modifiés depuis l’ouverture du vrak.
* Sauver l’espace de travail est une chose, mais il va falloir sauver chaque VRAK dans son asset correspondant.
	En effet, quand on fait des modifications dans notre VRAK,tout est enregistré de base dans celui-ci. Donc si on fait des modifications sur un asset dont le vrak est appelé en référence, les modifications se feront bien sur le vrak actuellement ouvert, et non sur la référence. si on veut récupérer ces modifications dans le vrak de notre asset, il va falloir déplacer nos modifications de notre vrak ouvert vers le vrak de l’asset.
 
Pour cela, on peut voir que si on sélectionne un asset dans la partie gauche, il y a des dossiers avec des opinions blanches, et des opinions grises dans la partie droite. 

.. image :: /images/vrak/v_opinions_level.png

Le dossier avec les opinions blanches, ce sont les modifications faites dans le vrak actuellement ouvert.
Le ou les dossiers avec les opinions grises, ce sont les modifications faites dans un vrak en référence
Dans l’image ci-dessus, on a le vrak de l’asset “Marlène” en référence. Et des modifications ont été faites sur le vrak ouvert.
- Pour déplacer ces opinions de dans le fichier VRAK de l’asset, on les sélectionne toutes (juste les petites ampoules, pas le dossier qui les contient) clic droit → move opinions

.. image :: /images/vrak/v_move.png

- On choisi le bon asset (on peut taper son nom dans la barre du haut pour le trouver plus facilement) et on clique sur “Move opinions”.
- clic droit dans la partie droite de notre vrak: refresh. On s'aperçoit que tout est devenu gris dans la partie droite: ça signifie que les opinions ont été déplacées, mais il y a une étoile * qui est apparue devant le nom de l’asset. L’étoile signifie que même si les opinions ont été déplacés, ils n’ont pas été enregistrés. Donc:
- clic droit sur l’asset gris avec l’étoile → save

Et en faisant un refresh à gauche, puis un à droite, l’asset réapparaît en gris, sans l’étoile. Il a bien été enregistré. 
Les Vraks non enregistrés apparaissent surlignés en rouge dans la fenêtre de droite. Si un Vrak a été sauvegardé mais qu’il apparaît toujours en rouge, il faut rafraichir le vrak. 

Quand vous avez enregistré le vrak qui était en référence, il faut aller dans Kabaret.
Le vrak est enregistré, mais il est locké par vous. il faut donc aller le publier.
dans Kabaret, aller dans la bank, dans l’asset, shading, stage > publish

.. image :: /images/vrak/v_k_publish.jpg

f) lancer un rendu:
===================
En haut de la fenêtre VRAK, on a cet icône:

.. image :: /images/vrak/v_render_button.jpg

Quand on clique dessus cette fenêtre s’ouvre :

.. image :: /images/vrak/v_render_select.jpg

Il suffit de double cliquer sur le render layer qu’on veut rendre, ou cliquer dessus puis de faire “use this one”.
Si on veut rendre le même render layer que le rendu d’avant on peut aussi cliquer sur “Use last one”

**********
II- ASSETS
**********

A- CREER UN SHADER
==================

a) Créer la base :
------------------
aller dans [Init Shading] dans la barre en haut de Vrak, selectionner le type ce que vous shadez (prop, char, ou set). Dans Asset name mettez le nom de l’objet ou du perso, et dans Shading name le nom du shader que vous voulez faire. 
Exemple : pour faire le pantalon de l’assureur, sélectionnez “CHAR”, Asset Name : InsurerMan et Shading Name : Pants

Un standard base se crée avec son sélector. 
Le selector se crée automatiquement avec ceci : $AssetName* and *ShadingName* 
Exemple : avec l’exemple ci dessus, on a le selector créé qui ressemble à ça : $InsurerMan* and *pants* 
à vous de voir si cela vous convient ou si vous avez des modifications à apporter dessus !
  
b) Créer la base tout à la main :
---------------------------------
Pour créer un shader, il faut se rendre dans l’endroit où il sera rangé :

	Root>ASSET>TYPE (CHAR/PROPS/SET)>Asset>SHADING

clique droit sur le dossier SHADING>create here
Trouver le *ShaderAssign* (on peut s’aider de la barre de recherche en haut)

.. image :: /images/vrak/v_create.jpg

Dans item name, mettre le nom du shader (Metal, Shoes, Gass, ect.) puis cliquer sur Create.
-Une fois le shader assign créé, cliquer droit dessus > create here
-Créer cette fois un matériau. Le plus simple, qui correspond au VrayMtl est le Standard Base. Dans item name, l’appeler Mat.
-Créer ensuite un selector. Le selector sert à choisir à quelles shapes seront assignées le matériau. Même opération : clique droit sur le Shader Assign > Create here : Selector_V2. L’appeler sel.
-dans le shader assign, il existe deux paramètres : shader et targets.
glisser dans shader le lien du Mat, et dans targets le lien du sel.
Vous pouvez aussi taper à la main dans shader : ./Mat et dans targets : @./sel 

Vous devez obtenir cette hiérarchie : 

.. image :: /images/vrak/v_sort.jpg

c) modifier le shader:
----------------------
Pour modifier le matériau, il faut faire clique droit sur Mat > execute et choisir les paramètres qui vous intéressent (create_diffuse_color, create_reflection_glossiness, etc.)

Une fois les paramètres exposés vous obtenez ceci : 

.. image :: /images/vrak/v_param.jpg

si vous voulez passer d’une couleur à une texture (ou l’inverse) pour un paramètre, faire :
clique droit sur le paramètre > change type >Vrak Shading Network > SN_BitmapText (ou SN_Color)

d) Remplir le selector :
------------------------ 
Prenons en exemple l’asset “Stylo”, avec une shape “bouchon” et une shape “base”

-Assigner le shader à tout l’objet : $UserAttribute* soit : $Stylo*
-Assigner le shader juste au bouchon : *shape* soit : *bouchon* (attention, si un autre objet a une shape bouchon le shader d’assignera aussi dessus)
-Assigner le shader au bouchon du stylo uniquement : $UserAttribute* and *shape* soit $Stylo* and *bouchon*
-Assigner le shader à plusieurs shapes, au bouchon et à la base : *shape01* *shape02* soit *bouchon* *base*
-Assigner le shader au bouchon et à la base du stylo uniquement : $UserAttribute* and (*shape01* *shape02*) soit $Stylo* and (*bouchon* *base*)
-Assigner le shader à TOUT le stylo SAUF le bouchon : $UserAttribute* and not *shape* soit $Stylo* and not *bouchon*

ATTENTION : RESPECTER LES MAJUSCULES DES USER ATTRIBUTES (pour les shapes les majuscules ne changent rien)


- Parmis les shaders les plus utilisés, il y a le Standar_Base (VrayMtl), le Skin_Base (SSS),  le SurfaceShader, le Blend_Base
- Pour avoir un specular, il faut obligatoirement exposer les paramètres : reflection_color et reflection glossiness
- pour les Bitmaptex de normal, il faut les passer en linear (et pas SRGB)

B- DUPLIQUER UN SHADER
======================

- Pour dupliquer un shader, il suffit de faire un clique droit sur le Shader Assign > Duplicate.
	Renommer le matériau et/ou changer le lien où vous voulez ranger le shader. 
	Attention, si le shader vient d’un autre Vrak, il faudra déplacer le shader dans le bon Vrak
- Petit exemple pour mieux comprendre : 
	Pour Dupliquer le shader Metal de la Bank_shaders (dans ANTESHADING) dans le SHADING du PROP “spotlight”
	ASSETS>ANTE_SHADING>Bank_Shaders>Metal     clique droite > duplicate
- Changer le lien du shader :

	.. image :: /images/vrak/v_duplicate.jpg

	Le shader venant du Vrak Default Shading, il faut déplacer le shader sur le bon vrak.
	Pour cela, il faut aller dans la fenêtre droite de Vrak, en ayant préalablement cliqué sur le nouveau matériau Metal. 
	Sélectionner toutes les opinions (Set et SetKind), faire dessus clique droit > move __ opinions. 
	Choisir le lien du Vrak dans lequel doit être mis le nouveau shader, dans le Vrak de la shad render de l’objet, ici :

	.. image :: /images/vrak/v_move_shd.jpg

- Hiérarchie des shaders : 
	S’il y a un problème de nommage, et qu’un asset ou une shape a plusieurs shaders d’assignés, c’est le shader placé le plus en bas dans la hierarchie qui prend le dessus. C’est pourquoi le **shading des assets peut overrider l’ANTE SHADING**. 
- Une fois le shader au bon endroit, changer le selector pour qu’il corresponde à l’asset et à la shape voulus en métal !

***********
III- RENDER
***********

a) les bases des Renderers
==========================
Chaque Render Layer a différents paramètres : 
-camera : y mettre le nom de la shape de la caméra désirée au rendu
-entities : y mettre les entités prises en compte. 

Un Exemple : ASSETS LIGHTING/Neutral_Lighting SETTINGS/ELEMENTS SETTINGS/HD
ASSETS : le render Layer prend les Shaders et propriétés qui se trouvent dans ASSETS
LIGHTING/Neutral_Lighting : Prend parmis les différents lightings existants dans l’onglet LIGHTING le set de lights “Neutral_Lighting”
SETTINGS/ELEMENTS : Permet de rendre toutes les passes de rendu
SETTINGS/ELEMENTS/RGB : Permet de rendre seulement les passes principales sans les XPASS
SETTINGS/HD : prend les réglages d’un render settings Haute définition

-En décochant la case Scène, on obtient des paramètres en plus dans les Renderers, notamment le Yeti. A mettre en 0 si on n’a pas de fur dans ce render layer et à 1 si on en a.
Une fois changé, recocher la case Scène.

-Le Render mask peut être utile si on veut vérifier que notre selector fonctionne correctement. 
On déplace le selector souhaité dans “shape”, et on rend avec render Mask. Il rendra seulement l’alpha des shapes sélectionnées dans le selector

.. image :: /images/vrak/v_renderers.png

b) créer de nouveaux Renderers
==============================
-Pour créer un nouveau render layer, on peut faire clique droit sur un render layer existant, et duplicate. donner le nom voulu au nouveau vrak et faire duplicate. 
Si le render layer que vous venez de dupliquer était appelé en référence depuis un autre vrak, il faut sélectionner les opinions à droite et les mover dans le vrak depuis lequel vous travaillez.
une fois les opinions movés, vous pouvez faire toutes les modifications souhaitées : changer le nom de la caméra, changer les settings, etc.

-Pour créer des renders layers avec des matte objects, ou des objets cachés, ou autre :

Pour être propre, il faut créer un dossier dans le Root, appelé “LAYERS”. 
Dans ce dossier, on créera différents dossiers selon les renderers qui nécessitent des paramètres spécifiques. Exemple : si vous avez un Renderer RGB_Char, qui necessite des paramètres spéciaux, créez un dossier LAYERS/RGB_Char. C’est dans ce dossier là qu’on rangera les paramètres qu’on créera.
Et pour que notre renderer prenne en compte ce dossier, il faudra le glisser dans l’entities du renderer.
exemple : si mon ancienne  entities était : ASSETS LIGHTING SETTINGS/ELEMENTS SETTINGS/HD
je viens rajouter mes paramètres et ma nouvelle entities est : 
ASSETS LIGHTING LAYERS/RGB_Char SETTINGS/ELEMENTS SETTINGS/HD

Il existe 3 paramètres qui sont les plus utilisés pour les renderers:
- le ShapesDelete :  il permet de supprimer un objet (et donc ses ombres).
- le RenderStat : il permet de cacher un objet, ou de lui enlever ses ombres.
- l’ObjectProperties : il permet de faire un matteobject. et sera utile pour récupérer seulement une passe d’ombres.

Pour les créer, il faut aller à la racine du root, clique droit, CreateHere. 
choisissez ce que vous voulez créer. Prenons comme exemple le ShapesDelete.
Dans item name, donnez le nom de l’action réalisée : DeleteSet. 
Si vous appelez votre paramètre ainsi, il se créera à la racine du vrak.
Pour créer un dossier et automatiquement bien ranger votre paramètre, vous pouvez taper ceci :


Chaque mot avant un / correspond à un dossier.
puis appuyez sur create.

Une autre solution est d’appeler votre item name “DeleteSet” et de faire create. votre item est alors à la racine du vrak. faites alors clique droit rename sur l’item. Et renommez “DeleteSet” en “LAYERS/RGB_Char/DeleteSet”. Il se rangera bien.

Dernière solution, c’est de créer un dossier dans la barre en haut de vrak avec le nom de votre ou vos dossiers. une fois créés, il suffit de faire clique droit dedans, create here et de créer le shapesdelete.



************
IV- SETTINGS
************

-Parmi les passes créées par Vrak on a les passes principales (RGB) et les Extra Passes (XPASS). Il manque la passe de vélocity, qui doit être créée dans les render settings > render elements de maya !

-Dans les XPASS il existe un dossier objects_selects. Dedans se trouvent des passes d’ID spécifiques au projet (souvent des masques des dents/peaux/yeux/cheveux des persos)
Si le dossier n’existe pas ou qu’il manque un masque, il faut faire clique droit > create here > ObjectSelect
Créer à partir de l’objectselect un selector dans lequel il faudra mettre le nom du ou des assets et/ou de la ou des shapes à rendre en mask
Un exemple : 

