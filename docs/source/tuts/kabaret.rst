.. _kabaret:

===========
Kabaret 2.0
===========

.. toctree::
    :maxdepth: 1

Démarrage
=========
Pour lancer Kabaret, il suffit d’aller sur le disque PROJECTS -> P:/ et de double cliquer sur KABARET.bat

    .. image :: /images/kabaret/k_start.jpg


L’accueil
=========
L’interface d’accueil est la suivante:

    .. image :: /images/kabaret/k_index.jpg

| Elle présente la liste de tous les projets actuellement dans la base de donnée.

Les contrôles
=============
| Les contrôles de base sont les suivants:

    - Clic gauche : sélection
    - Clic droit : option/menus déroulants
    - Clic centre : sélection
    - Clic centre drag : contrôle la taille d’une fenêtre de liste
    - Ctrl+Clic gauche : ajoute à la sélection
    - Maj+Clic gauche : sélection multiple
    - Double clic: ouvre la section dans la vue courante
    - Ctrl+Double clic : ouvre la page dans une nouvelle vue

Les vues
========
Il est possible d’avoir plusieurs en même temps.

    .. image :: /images/kabaret/k_views.jpg

| Elles peuvent être sur des pages différentes ou sur la même page.
| Elles peuvent être déplacées en utilisant le drag&drop depuis la barre rouge.
| L'icône “*” permet de dupliquer la vue.
| L'icône “+” permet de maximiser la vue.
| L'icône “-” permet ensuite de revenir à la vue initiale.

La navigation
=============
Pour naviguer entre les pages il y a plusieurs manières.

    .. image :: /images/kabaret/k_navbar.jpg

| Ces boutons permettent, dans l’ordre, de retourner à la page précédente, aller à la page parente, retourner à la page suivante ou de retourner à l’accueil des projets.
| Il est aussi possible de cliquer sur une partie du chemin de la page pour accéder à cette page. 
| Le Ctrl+double clic fonctionne aussi dans le chemin.
| Le clic droit sur une partie du chemin permet dans la partie supérieure d'aller sur une page jumelle (exemple : P001 > P002) et dans la partie inférieure, sur une page enfant de ce chemin.

.. tip::
    Par exemple, je peux ouvrir une nouvelle vue avec la racine du projet en ctrl+double cliquant sur la première partie du chemin.

.. image :: /images/kabaret/k_arbo.jpg

| Comme dans toutes les arborescences, le bouton "+" permet de développer le niveau suivant de l'arborescence.
| Le bouton "-" fera l'inverse ^^ !

L'icône suivante permet d'accéder aux actions des niveaux sous-jacents.

    .. image :: /images/kabaret/k_actions.jpg

Ici, l'icône donne accès à l'action "Open in VRak", c'est un raccourcis vers le bouton du même nom tout en bas du niveau suivant.

    .. image :: /images/kabaret/k_action_level.jpg

Les projets ORGAN
=================
| "ORGAN" est le nom de l'organisation du flot de travail utilisé pour la plupart des projets.
| Il est structuré comme sur l'image suivante.

    .. image :: /images/kabaret/k_organ_structure.jpg

Open
----
| C'est là que se trouvent les boutons de lancement des logiciels disponibles à Supamonks
| Lancer un logiciel depuis ces boutons le démarre avec l'environnement du studio mais hors contexte.
| Certaines fonctionnalités sont manquantes car beaucoup sont spécifiques au contexte comme celui des département de modeling, de setup ou d'animation etc...

Vraks
-----
Cette page regroupe 3 pages de fichiers vrak:

    - Default Shading
    - Default Lighting
    - Render Settings

| Les pages de fichiers sont conrtuites de la même manière, quelle que soit le type de fichier : :ref:`file_page`

| Ici les fichiers sont pris en référence en premier dans tous les plans d'un projet.
| Ils définissent le premier niveau d'execution de Vrak.
| Les opinions des paramètres de ces fichiers seront surpassées par toutes celles venant d'autres fichiers Vrak du système.
| Pour plus d'informations, veuillez vous référez au tutos :ref:`vrak`.

Banks
-----
| C'est là que se trouvent les assets.
| Si tu travail à l'asset, ceci est ton territoire.

Films
-----
| C'est là que se trouvent les scènes de travail des films.
| Si tu travail au film, à la séquence ou au plan, ceci est ton territoire.

Edith
-----
| C'est là que se trouvent les montages, leurs sources et leurs sorties.
| Si tu veux voir la dernière sortie du film c'est par ici.

Muster
------
| Pour être originaux on a mis un soft à part, c'est le dispatcher, le soft qui gère la ferme de rendu.
| Pour gérer les rendus c'est par ici.

Admin
-----
| Ce sont les paramètres du projet
| Ils sont gérés par les directeurs de productions et les superviseurs.
| Attention à ne pas faire trop de bordel là dedans ;)

Notions
=======

Les quelques notions suivantes vous permettront de mieux apréhender le concept de Kabaret.

.. _project_settings:

La paramétrisation de base d'un projet
--------------------------------------
| Pour bien démarrer un projet il y a plusieurs choses à définir.

Les settings du projet
----------------------
- **Les paramètres globaux du projet**. 
    Dans Settings->Image, il est important, dès le début du projet, de définir les paramètres de frame rate et résolution. 
    Ces paramètres seront utilisés par défaut à tous les niveaux du projet.
- **Les paramètres par défauts des banques du projet**. 
    En double cliquant sur "banks", on trouve les paramètres "Default Bank" et "Default Cam". 
    Il faut donc cliquer glisser la banque principale et la caméra du projet.
- **La caméra**. 
    Un projet Organ s'articule autour de l'utilisation d'une seule caméra pour tous les plans d'un projet. 
    Il faut donc créer un asset "Camera" et avoir dans la scène maya de "For Anim" une caméra nommée "camera". 
    En respectant cette casse les paramètres par défauts du reste du pipe permettront la bonne exécution du flow.
    Ensuite il est faut modifier le paramètre "Render Cam name" dans "Asset Config" et mettre "AnimBake:camera".

.. warning::
    Les paramètres modifiés le sont pour tout le monde, instantanément et jusqu'à nouvelle modification.

.. _arch_page:

Une architecture recurrente
---------------------------
Mod, Setup, et Actor, ont une architecture commune.

    .. image :: /images/kabaret/k_arch_asset.jpg

Sur ces trois pages se trouvent des éléments récurrents de Kabaret:

    - Status
        Permet de définir le statut de l'asset : WAIT (en attente), INV (dans l'inventaire), WIP (work in progress), OOP (out of production) puis RVW (to review), DONE (finit), RTK (retake)
    - La scène
        Voir ci dessous :ref:`file_page`
    - On validate Options
        Définit le comportement du pipe lors de la validation de l'asset.

Certaines pages utilise cette base avec quelques ajouts comme pour la page Actor qui donne accès à la For Anim, Static Cache et leurs options.

.. _file_page:

Les pages de fichiers
---------------------

    .. image :: /images/kabaret/k_scenes.jpg

Les scènes, qu'elles correspondent aux fichiers maya, nuke ou autres ont toutes la même base.

    - Filename
        Donne le nom du fichier de base sur le disque.
    - Exists
        Indique si le fichier existe bien.
    - Explore
        Ouvre l'explorateur windows à l'emplacement du fichier.
    - Is Reversioned
        Indique si le fichier est correctement versionné.
    - Add To Reversion
        Permet de versionner le fichier.
        A n'utiliser que si ce n'est pas déjà le cas.
    - Locked By
        Indique si le fichier est actuellement verrouillé, donc en cours d'utilisation, et par qui.
    - Steal Work
        Permet de prendre le Lock.
        A n'utiliser qu'après avoir vu avec le propriétaire, le superviseur ou le directeur de production.

    .. _history:

    - History
        Donne accès à l'historique du fichier.

        .. image :: /images/kabaret/k_history.jpg

        La version rose (WORK) indique que le fichier est utilisé par "Max", cette version n'est qaccessible que par cet utilisateur.
        La version verte (VALID) est la version validée, c'est celle utilisée pour la suite du pipe.
        Les versions blanches sont des version publiées.
        Les version grise sont visible en utilisant l'action "Toggle Revisions" de la flèche à côté de "History", ce sont toutes les sauvegardes des utilisateurs.

| Pour ouvrir une scène il suffit de cliquer sur le bouton "Open in Maya".
| Cette actions lancera l'ouverture de la scène accessible la plus récente.
| Dans cet exemple, tous les utilisateurs accederont à la version validée.
| Seul l'utilisateur "Max" accèdera à la version WORK en utilisant ce bouton.

| Sur chaque version de l'historique il est possible d'acceder à certaines actions.
| Parmis elles, l'action "Open" permettra d'ouvrir spécifiquement le fichier correpsondant à cette version.

| Sur chaque version il est possible d'exécuter des actions, disponible dans le clic droit.
| Notamment l'ajout d'une version particulière.

    .. tip::
        | Avec l'action Add Version disponible dans le clic droit je peux accéder à une fenêtre qui me permettra de créer une version avec un nom spécifique.
        | Par exemple sur ma scène de Mod je fais un Add Version et entre "EyesClosed". J'ai maintenant la possibilité de référencer une scène maya ayant pour préfixe "EyesClosed" qui sera différente de la version "VALID"
        | Autre exemple : sur mon layer de rendu j'ajoute une version "MotionBlur", je pourrais alors utiliser cette version motion blurée dans la suite du pipe ou celle de la "VALID" qui est sans motion blur.

.. warning::
    - Il ne faut pas travailler sur une version ouverte depuis l'histroique.
    - Ce contexte particulier doit être principalement utilisé pour certaines actions.
    - Ce contexte doit être utilisé pour regarder dans une scène ou la sauvegardée en étant propriétaire du lock ou sur une scène publique.

| Tous les fichiers versionné seront automatiquement incrémentés par le système du pipe.
| Chaque sauvegarde (Ctrl+S) sera accessible via l'historique.

.. danger::
    - Il est totalement interdit, de sauvegarder manuellement un fichier sous un autre nom.
    - Il est totalement interdit d'activer l'incrémentation automatique des fichiers dans les logiciels.

L'état du contenu de la page
----------------------------

    .. image :: /images/kabaret/k_summary.jpg

| Pour certaines pages et à certains niveaux se trouvent les résumés des états des contenus de la page.
| Ici, à côté de Mod, on peut voir que les contenus sont indiqués comme étant "Ready for edit".
| Cela signifie que la scène contenu dans cette page, existe et qu'elle est correctement versionnée.

| L'étape de Facial est en état : "File does not exists" et "Not Yet Reversioned"
| Les fichiers contenus n'ont jamais été initialisés. Ils n'existent pas et ne sont donc pas versionnés.

| L'étape de setup n'a pas été initialisée correctement. Il n'est indiqué **que** "Not Yet Reversioned".
| L'absence de mention "File does not exists" implique que la scène existe bien.
| Elle n'est simplement pas versionnée.

| Les différents états sus mentionnés au niveau supérieur sont des attributs de l'état de la scene.
| Ils apparaissent donc sous la scène.
| Nous pouvons voir ici, dans la scene de setup, l'attribut "Exists" coché et l'attribut "Is Reversioned" décoché.

.. important::
    | Dans cet état la scène de setup ne doit pas être utilisée pour travailler.
    | Elle ne peut pas non plus être utilisée par le pipe.
    | Il faut corriger cet état en utilisant le bouton "Add To Reversion" qui va versionner le fichier.

| L'état "Validation Needed" signifie que la scène publique la plus récente n'est pas validée.

    .. image :: /images/kabaret/k_valid_state.jpg

| Cela ne signifie pas forcément qu'aucune version Valid n'existe.

Le rafaichissement
------------------
Plusieurs actions ne rafraichissent pas correctement les informations affichées.

| Par exemple, l'initialisation d'une scène peut ne pas rafraichir le résumé de son état ou son historique.
| Pour rafraichir les informations il suffit de cliquer sur la nom de la page courante dans le chemin affiché tout en haut.

Les Banks
=========

| Il est possible d'utiliser plusieurs banques pour un projet.
| Mais il est conseillé d'utiliser uniquement la banque principale "Bank".

| Les actions de la page "Banks" proposent la création ou la suppression d'une banque.

.. important::
    La gestion des banques est réservée au superviseur.

.. _bank:

Bank
----

| Une banque contient tout d'abord une section "Shared Vsl"
| Dans cette section sont accessibles des pages qui offre la possibilité de mettre en place une banque de shading pour le projet.

Tout ça étant lié à :ref:`VRAK <vrak>` et son système de shading :ref:`VSL <vsl>`

| Ici, nous avons la possibilité de générer un fichier VSL, nommé "Bank Vsl" qui pourrait contenir une quantité de matériaux.
| Les textures et fichiers nécessaires au shading d'un shader utilisé par plusieurs assets du projet sont à ranger dans le(s) dossier(s) de "Bank Folder".
| Les contraintes du système de VSL peuvent nécessiter l'utilisation de plusieurs fichiers VSL pour gérer la banque.
| La page "Extra" permettra donc de générer autant de fichier VSL que nécessaire.

Assets
------

| Il s'agit de la liste des assets de la banque.
| A l'aide du champ "Filter..." il est possible de filtrer les noms des assets.
| Les entêtes "Name" et "Type" permettront de classer les assets par nom ou par type.

| Les actions disponible sur cette page sont des actions de création d'asset.

.. important ::
    La création d'assets ne doit être faite que par un superviseur ou un dir de prod.

Les assets
==========

    .. image :: /images/kabaret/k_asset.jpg

Les thumbnails
--------------
| Il est possible de mettre une vignette pour chaque asset.
| Pour cela, il suffit d'appuyer sur la petite flèche qui donnent accès à 2 options : 

    - Take a snapshot
        Ouvre une fenêtre à redimensionner et placer pour faire un snapshot à l'aide du bouton capture et l'utiliser comme vignette
    - Copy path from clipboard
        Porte bien son nom ^^

Usable
------
Définit l'état de l'asset.

.. warning::
    Cet attribut est important ! 
    **Activé**, il autorise le pipe à prendre l'asset en référence dans les scènes maya des plans qui ont cet asset au casting.
    **Désactivé**, l'asset peut être ajouter dans tous les castings, les scènes peuvent être initialisée, le pipe n'autorisera pas à prendre l'asset en référence dans les scènes maya.

.. danger::
    Activer cet attribut sans que la scène de "For Anim" soit initialisée ou si elle comporte des erreurs, maya aura une référence vide qui ne pourra pas être suprrimée ou modifiée.
    Si du travail est fait dans une scène maya avec une ou plusieurs référence vide le fichier sera compromis !

Comment
-------
Permet d'écrire un commentaire sur l'asset, il pourra être consultable plus loin dans le pipe.

Mod
---
| Contient la scène de modélisation, son historique et ses options.
| Le bouton "Initialize" lance l'initialisation d'une scène maya vide.
| Il y a alors 2 possibilités :

    - Reset The Mod Scene
        Comme l'intitulé l'indique, tout travail effectué dans la scène de setup sera perdu, écrasé par le contenu de la scène de mod.
    - Cancel
        Qu'elle existe ou non elle sera créée ou remplacée par une scène vide.

.. _mod_scene_state:

| Une fois dans l'environnment maya ouvert à ce stade, sont accessible des actions particulières:

    - Supa Save
        Mode de sauvegarde liée au pipe avec possibilité de Valider la scène lors de la sauvegarde.
    - Add to BKSN
        Ajoute les objets sélectionnés dans le set _BKSN. Tous les objets contenus seront bakés entre l'animation et le lighting. 
        Les objets qui ne le sont pas n'arriveront donc pas au lighting.
    - Remove From BKSN
        Permet de retirer les objets sélectionnés du set BKSN.
    - Add to Static BKSN
        Ajoute les objets sélectionnés dans un set _BKSN_STATIC.
        Les objets sélectionnés seront exportés dans un alembic d'une frame à la validation du setup. L'export sera réinjecté dans la scène de lighting.
    - Remove From Static BKSN
        Permet de retirer les objets sélectionnés du set BKSN_STATIC.

Facial
------
| Contient la scène de Facial, son historique et ses options.
| Le bouton "Initialize" lance l'initialisation d'une scène maya vide.
| Il y a alors 2 possibilités : 

    - Reset From Mod Scene
        Comme l'intitulé l'indique, tout travail effectué dans la scène de hair sera perdu, écrasé par le contenu de la scène de mod.
    - Initialize with Empty scene
        Qu'elle existe ou non elle sera créée ou remplacée par une scène vide.

| Une fois dans l'environnment maya ouvert à ce stade, sont accessible des actions particulières:

    - Supa Save
        Mode de sauvegarde liée au pipe avec possibilité de Valider la scène lors de la sauvegarde.

.. note::
    L'étape de Facial est actuellement gérée manuellement par le setupeur, en accord entre le setupeur du corps et celui du facial.

Setup
-----
| Contient la scène de setup, son historique et ses options.
| Le bouton "Initialize" lance l'initialisation d'une scène maya vide.
| Il y a alors 2 possibilités : 

    - Reset From Mod Scene
        Comme l'intitulé l'indique, tout travail effectué dans la scène de setup sera perdu, écrasé par le contenu de la scène de mod.
    - Initialize with Empty scene
        Qu'elle existe ou non elle sera créée ou remplacée par une scène vide.

| Une fois dans l'environnment maya ouvert à ce stade, sont accessible des actions particulières: :ref:`Maya Mod Actions <mod_scene_state>`

Hair
----
| Contient la scène de Hair, son historique et ses options.
| Le bouton "Initialize" propose d'accéder aux options de configuration de l'asset.
| Cette scène est générée automatiquement par le pipe.

Shad Render
-----------
| Cette page contient les scènes relatives aux shading d'un asset.

Actor
-----
Cette page contient les scènes de construction de l'asset utilisée dans le pipe.

Sources
-------
| Permet de générer un dossier non versionné sur le disque.
| Il peut être utilisé pour stocker tout type de données concernant l'asset.

Asset Config
------------
| Donne accès aux paramètres de configuration de l'asset.
| Ces paramètres sont automatiquement configurés par le pipe.

Les films
=========

La bank
-------
| Au niveau de chaque film, il est possible de créer et gérer une banque dans la "Bank" de celui ci.
| La gestion de la banque se fait alors comme dans une banque de projet :ref:`bank`

.. note::
    | Les projets **Quick** et **BK** utilisent le système des banques de films.
        | Dans la banque du projet sont regroupés les assets partagés entre un ou plusieurs films.
        | Dans la banque d'un film sont regroupés les assets de film

Le Vrak
-------
| "Shared Vrak" propose de générer et gérer le fichier Vrak du film.
| Voir :ref:`VRAK <vrak>` et :ref:`Le pipe de Vrak <vrak_pipe>`

Les séquences
-------------
| Au niveau du film, apparait la liste de toutes les séquences qu'il contient.

| Chaque séquence est composée d'une page Layout, une liste de Shots, et d'un Stage Vrak.

.. _layout:

Le layout
^^^^^^^^^

    .. image :: /images/kabaret/k_layout.jpg

| Cette page est une page fichier qui donne accès à la scène de layout d'une séquence.
| Le layout utilise son propre casting qui doit contenir tous les assets de la séquence.

- La page blast donnera accès au blast validé et à tout l'historique de playlast disponible, exécutés à l'aide du Supablast disponible dans l'environnement maya de la scène.

- L'action "Edit Shots" permet d'ouvrir l'utilitaire d'édition de shot.

    .. image :: /images/kabaret/k_edit_shots.jpg

    | L'éditeur affiche dans la partie supérieure, le casting du layout et dans la partie inférieure, les plans du films.
    | Pour voir les plans s'afficher, il faut les avoir préalablement créés.
    | Les boutons au dessus et à gauche des plans permettent de revoir les durées et l'organisation du montage.

    .. tip::
        | L'édition des durées de plans est difficilement précise dans l'interface de l'éditeur.
        | Modifier préalablement les ranges des plans dans leurs section :ref:`Montage de plans <shot_montage>` permet d'avoir dans l'éditeur les plans déjà bien organisés.

    | L'utilité principale de l'éditeur réside dans la répartition du casting dans les plans.

    | L'éditeur est accessible dans maya.

    | En sélectionnant un plan, les assets contenu dans son casting sont surlignés dans la liste du dessus.
    | Si rien n'est surligné, le casting du plan ne contient aucun asset du casting du layout.
    | En sélectionnant un ou plusieurs assets 2 actions sont accessible à l'aide du clique droit.
    | Il est alors possible d'ajouter ou supprimer les assets du casting du plan sélectionné.

    | Cela permet donc de répartir les assets dans les plans de la séquence.

- L'initialisation de la scène génèrera une scène maya prenant en référence toutes les scènes ForAnim des assets du :ref:`casting <casting>`.

.. _layout_montage:

- La page Montage :

    .. image :: /images/kabaret/k_montage.jpg

    | Elle permet de spécifier les ranges de la scène, ici de layout.
    | Les paramètres principalement utilisés sont "First" et "Last" qui définissent respectivement la première et la dernière frame de la scène.

- La page "Split" permet, à l'aide de l'action "Submit" de lancer le split du Layout.
    | Le pipe va alors découper la scène de layout en fonction des castings et des montages dans plans de la séquence.
    | Pour chaque plan, la scène d'anim sera générée en récupérant la scène de layout et en laissant activée les références des assets présents dans le casting du plan.

La liste des plans
^^^^^^^^^^^^^^^^^^
| La sequence contient la liste des plans : "Shots"
| Les actions de "Shots" permettent de créer un ou plusieurs plans.

Le stage de la sequence
^^^^^^^^^^^^^^^^^^^^^^^
| "Override Stage" est le nom du stage Vrak de la sequence.
| Il sera inclus dans tous les plans après celui du film et ceux de la banque et du projet.
| Cela permettra de faire des overrides à la séquence.
| :ref:`Voir Vrak <vrak>` 

Les plans
---------
Tous les plans ont la même structure.

.. _shot_montage:

- Le montage du plan
    | Les paramètres sont les mêmes que pour toutes les autres pages de montage (:ref:`Layout Montage <layout_montage>`) mais sont ici utilisés pour le plan.
- "Summaries"
    | Une page de résumé de l'état des scènes composant le plan
- Anim
    | La page de la scène d'anim.
    | Elle est issue de la même base que la page de layout : :ref:`layout`

    - Des actions raccourcies comme Open Anim et Open Blast
    - La page de la scène
    - La page de Blast
    - La page Initialize
    - Les On Validate Options

- Bake
    | La page contient l'alembic de la scène d'anim et une scène maya qui contient cet alembic.
    | La scène maya sera prise en référence dans la scène de lighting.
    | Les sections "Update" permettent de réinitialiser les scène correspondantes.
- Lighting
    | La page permet d'accéder à l'étape de lighting.
    | Elle contient :

        - La page du fichier le scène maya
        - La page du fichier de Casting Vrak
        - La page du fichier Vrak du plan
        - La page d'initialisation des fichiers précédents
        - Les On Validate Options
        - La page des Layers rendus

            .. _render_layers:

            | Chaque layer de rendu est sorti dans un dossier séparé.
            | Chaque layer est versionné (:ref:`History <history>`)
- FX
    | C'est une section particulière qui permet de gérer les scènes de FX d'un plan.
    | Elle est décomposée en 3 espaces.
    - In Files
        Cette section permet de générer des dossiers dans lesquels seront ranger les données entrantes des scènes de FX.
    - Scenes
        | Les actions de cette page permettent de créer des fichiers de différents type selon les besoins.
        | Chacune des scènes aura une page de fichier avec un historique. L'absence d'action d'initialisation sera contournée en faisant un Add to Reversion qui forcera la génération de la scène.
    - Out Files
        | Cette section permet de générer des dossiers dans lesquels seront ranger les données sortantes des scènes de FX.
        | Typiquement des fichiers Alembic ou Vdb
- Comp
    | Cette page propose d'accéder aux scènes de compo Nuke et contient en plus deux pages : "Render Folder" et "Images"
        - Images
            Est une sorte de raccourci vers les dossier de sorties des rendus Nuke de la version "VALID"
        - Render Folder
            Est une page qui donne accès eux différentes sorties de Nuke et à leur historique.

            .. attention ::
                | Contrairement aux layers de rendu du lighting qui sont versionnés séparément les uns des autres, les sorties de Nuke sont versionnées toutes ensemble.
                | Donc tous les nodes Write d'un fichier Nuke génèreront des images dans un dossier par node Write mais tous ces dossiers seront versionnés ensemble.
                | En conséquence, il faut relancer les rendus de tous les nodes Write à chaque fois que l'on souhaite en mettre un à jour.
- Polish
    | Cette page permet d'accéder à la scène de Polish du plan.
    | C'est une scène After Effect dont l'architecture est la même que pour le compo Nuke.
    | Il n'y a pas d'action d'initialisation donc pour générer le fichier il faut faire un Add To Reversion.

    | Comme les rendus se font en local, le processus est le suivant:

        - Créer la scène en faisant Add To Reversion
        - L'ouvrir, si il y a une erreur, bien faire un Save As sur le fichier indiqué dans le "Filename" sur la page de la scène.
        - Une fois le contenu prêt, faire un Ctrl+S pour ne rien perdre ;)
        - Dans Kabaret, utiliser l'action "Publish" pour publier et valider la scène.
        - Cette action créera une nouvelle version de sortie.
        - Dans les actions du "Render Folder", l'action "Show Path" donne un chemin qu'il suffit de copier et de coller comme chemin de sortie dans After.
        - **Attention** : Il faudra ajouter des crochets "[]" autour des "#" pour que tout soit correct.
        - Ensuite il suffira de lancer le rendu.
        - Pour faire une nouvelle version il suffira de publier et valider la scène.
        - Si les étapes précédentes sont bien respectées, il suffit de copier la sortie dans la render queue d'After et de lancer le rendu.
        - Il sera automatiquement dans la nouvelle version.

.. _casting:

Les casting
-----------
C'est la liste des assets du plan ou de la scène.

Les Edith
=========
| C'est la section des montages du projet.
| Il est possible de créer plusieurs Edit, typiquement un par film.

| Chaque Edit peut avoir plusieurs Cut.
| Chaque Cut est composé de plusieurs pages.
    
    - Comp
        Une page scène After Effect.
    - Render
        Une section permettant la création de dossiers destinés à recevoir les différentes itérations du montage.
    - Source Files
        Une section dans laquelle on peut ranger les fichiers sources.
    - Sound Files
        Une section pour ranger les fichiers sons utilisés dans le montage.

Muster
======
| Cette page regroupe plusieurs choses:

    - L'action "Open Console" qui permet de lancer Muster.
    - La page "Config"
        | Elle donne accès aux attributs de configuration de Muster.
        | Seul le bouton Test Connection est utile pour l'utilisateur. Il permet de vérifier que l'utilisateur courant à bien accès à Muster.

| Pour l'utilisation de muster, veuillez vous reporter à ce document : :ref:`Muster <muster>`

Settings et Admin
=================
| Ces pages contiennent tous les paramètres de réglage globaux du projet.
| La partie "Settings" ayant été abordée :ref:`ici <project_settings>`
| La partie "Admin" étant réservée aux administrateurs.
