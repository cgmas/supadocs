.. contents::
   :depth: 3
..

**************************
VRAK pour Bande De Sportif
**************************

| **I/**\ **Préambule**\ **:**

a. **Présentation de VRAK**:
   VRAK est un outil développé en interne à Supamonks. Il permet
   d’automatiser certaines tâches afin d’accélérer la production mais
   aussi d’avoir un rendu plus homogène. A l’étape du shading, VRAK va
   attribuer un shader en fonction d’un nom de mesh. Si tout est nommé
   correctement, le shading devient plus rapide et efficace.
b. **Conseils de nommage:**
   \* On peut nommer les meshs en fonction de leur matière.
   *ex*: Pour un décors, on va nommer de façon classique les objets
   métalliques, mais on va lui mettre un préfixe “metal”, pareil pour le
   bois ou le plastique par exemple.
   \* On peut aussi donner, dans le nom du mesh, une indication pour
   savoir s’il a une map ou non.
   *ex*: Sur un perso, il y a des éléments en cloth. Certains auront une
   map, d’autres auront juste une couleur.
   Pour ceux ayant une map: cloth_map_nomdumesh_msh
   Pour ceux étant juste jaune (sans map): cloth_yellow_nomdumesh_msh
   Pour ceux étant juste bleu (sans map): cloth_blue_nomdumesh_msh
   Ect …
   Mais **ATTENTION**! Si on nomme un mesh juste: cloth_nomdumesh_msh,
   on risque d’être embêté après dans VRAK. Parce que le dénominateur
   “cloth” est aussi présent dans “cloth_map” ou “cloth_yellow”. Il
   prendra donc le shader du plus bas dans la hierarchie (voir III/b.)
   \* Enfin on peut aussi compter sur l’ANTE SHADING (voir III/a.), en
   nommant correctement certains mesh classiques comme les arbres ou les
   cailloux:
   buissons ⇒ shrub
   collines ⇒ Hill
   herbe ⇒ grass
   Rochers ⇒ Rock
   ECT… Voir les selectors de l’ante shading pour ne pas faire d’erreur.
   **NB**: VRAK différencie les majuscules des minuscules. Il est
   préférable de tout écrire en minuscule (Comme ça, on n’a pas à
   réfléchir s’il y en a ou pas, et on gagne du temps).

| 
| **II/**\ **Quelques étapes avant d’utiliser concrètement Vrak**:

a. **Validation de l’asset:**
   \* Publish Valid de la scène de setup.
   \* Mettre les petits shaders dans la shad Anim pour les animateurs
   (Ne pas oublier les dents et les switch de dents!)
   \* Mettre un Vray Material sur tous les polygons dans la scène de
   Shad Render.
   **NB**: Pour les décors en extérieur: on importe le sky (dans
   shared), scale à 100, on ne supprime pas le contrôleur, on le place
   dans le BDD de l’asset.
   Penser à aller chercher la texture de sky pour la shad anim.
b. **UCSL Anim** (demander à Pink de le créer):
   \* Dans la scène d’anim d’UCSL (Use Case Shading Lib), on va importer
   tous les assets qu’on a fait.
   \* CTRL + SHIFT + R ⇒ Ca ouvre une fenêtre qui va nous permettre
   d’importer nos références.
   On clique sur un des gros boutons en bas de la fenêtre de type ACTOR,
   ou DECORS ou PROPS (selon les cas).
   On fait clic droit dans les cases “Assetfamily” et ensuite “asset”,
   et on fait “add ref”.
   Si l’asset en question a des switch:
   *ex*: Theo a plusieurs tenues, il faut importer l’asset autant de
   fois qu’il y a de switchs:Theo en maillot, Theo en tenue de plongeur,
   Theo avec une serviette autour du cou ⇒ on importe l’asset Theo 3x
   **NB**: Il faut faire close de cette fenêtre de reference avant de
   pouvoir se déplacer dans la scène.
   **NB**: Si les assets sont tout vert, pas d'inquiétude, c’est un
   problème de display. Il suffit de les smoother avec 3 pour qu’ils
   redeviennent normaux.
   **NB**: On place les assets en ligne les uns à côté des autres. On ne
   déplace pas le décor! S’il y en a plusieurs (de décors), on fait
   switcher la visibilité d’une frame à l’autre.
   \* On n’oubli pas d’importer une caméra! Pour cela, il faut juste
   cliquer sur CAM et add ref.
   Pour la suite, il est plutôt utile à cette étape de keyer sa caméra
   en ayant différents angles de vue de nos assets pour pouvoir les
   shader.
   *ex*: frame 1: persos vus de face; frame 2: persos vus de dos; ect…
   \* On reduit la time line au nombre de frame dont on a besoin (nombre
   de keys sur la CAM), ça permet un calcul plus rapide lors de la
   validation de cette scène.
   \* CTRL + SHIFT + S ⇒ OK, bake and cook et force initialisation du
   lighting.
   **NB**: Lors du calcul, les erreurs de caméra sont normales, ne pas y
   prêter attention.
c. **UCSL de lighting**:
   \* La scène d’UCSL dans le dossier lighting est créée. On va faire le
   shading dans cette scene. On peut voir qu’en dessous d’elle, elle a
   un fichier Vrak, locké par l’utilisateur.
   **NB**: Si on doit rajouter d’autres assets par la suite, ou s’il y a
   eu des modifications de modé, d’UVs ou de renommage, il va falloir re
   bake-and-cooker et re force-initaialiser la scène d’anim, MAIS! Avant
   cette validation, il faut publisher le Vrak de la scène de lighting
   correspondante. Sinon, on ne pourra pas enregistrer nos modifications
   de shading.
   \* Avant de commencer quoi que ce soit, on clique sur l’icone des
   render settings (RS) une première fois ⇒ message d’erreur. On clique
   une deuxième fois, les RS s’affichent: On va dans l’onglet Vray,
   camera, et on décoche la DOF et le Motion Blur.

| **III/**\ **VRAK**:

a.  **Interface**:
   \* Pour ouvrir l’interface de VRAK, on va tout en haut dans maya,
   dans la barre de menu, tout à droite [SMKS] → Lighting → Show Vrak
   \* Si on double clique sur le root, on voit qu’il y a plusieurs
   onglets.
   La colonne de gauche, c’est “l’espace de travail”.
   La colonne de droite, ce sont les modifications créées, qu’il faudra
   enregistrer.
   \* L’Ante Shading: On ne modifie rien dans cet onglet! Mais on peut
   l’ouvrir pour regarder ce qui va être ante-shadé, cad qui aura déjà
   un shader sans qu’on ait rien à toucher. Ca peut nous aider à nommer
   correctement nos mesh dans la scène de setup.
   \* Render Mask:
   Cela peut être utile si on veut vérifier que notre sélector
   fonctionne correctement.
   On déplace le selector souhaité dans “shape”, et on rend avec render
   Mask (voir III/d.)
b. **Principe**:
   \* On va devoir créer des Assets, par famille, comme indiqué dans le
   google doc de l’épisode.
   **NB**: On peut remarquer que sous le fichier Shad Render (dans le
   SupaBrowser) d’un asset, il y a un petit fichier VRAK. C’est là que
   seront enregistrées les informations de shading de cet asset (Voir
   III/e.)
   On verra comment faire dans VRAK pour créer ces assets dans c. Ici,
   voyons comment ils fonctionnent:
   Dans chaque asset il y un ou plusieurs [ASSIGN, SELECTOR et SHADER].
   \* Dans un ASSIGN, il y a 2 chemins:
   target: selector correspondant
   shader: shader correspondant.
   On évite de toucher à cet élément ASSIGN, étant donné que tout est
   déjà OK lors de la création d’un asset dans vrak.
   \* Dans SELECTOR, on va nommer les mesh à qui on souhaite attribuer
   le Shader (voir I/b). ça se présente de cette manière:
   $NomDeAsset \*
   Le “$” signifie que ça ne va s’appliquer QUE à l’asset qui suit.
   *ex*: si on a Louis et theo dans la même scène, que chacun porte la
   même tenue (on a fait un copié/collé des tenues dans les scènes de
   setup), ils ont tous les deux les mêmes noms de mesh.
   Pour que l’assignation des shaders correspondant à Theo, ne
   s’applique QUE à Theo, on le signale au selector: $Theo
   Ce qui est entre étoiles \* ce sont le nom des mesh, s’il y en a
   plusieurs, on les mets entre parenthèse.
   *ex*: pour du metal: $Theo \*metal\*
   Pour du plastique: $Theo (*casque\* \*genouillere\* \*coudiere*)
   **NB**: S’il y a un problème de nommage, c’est le shader placé le
   plus en bas dans la hierarchie qui prend le dessus. C’est pourquoi
   l\ **e shading des assets peut overrider l’ANTE SHADING**.
   \* Dans SHADERS, on a un shader qui de base est un concrete. Il va
   s’assigner sur son selector.
c. **Créer son Asset**:
   \* Pour créer un asset dans VRAK, on va créé un de ses shaders.
   Un asset peut avoir plusieurs shaders.
   En créant le premier shader, on créé automatiquement l’asset.
   Pour cela, il faut cliquer sur le bouton en haut de la fenêtre VRAK:
   [init shading]
   Dans cette fenêtre, n’oublions pas de cliquer sur l’Asset type!
   Actor, Decors ou Props. Sinon, vrak va mal nous ranger l’asset dans
   la hierarchie.
   On nomme correctement: nom de la famille, nom de l’asset, nom du
   shader.
   **NB**: Sub Group: Cela peut être utile si un personnage (comme Theo
   généralement), a plusieurs tenues. On écrit ici le nom de la tenue et
   c’est mieux organisé après.
   \* Une fois le shader créé, la première chose à faire est de
   paramétrer le selector (voir III/b)
   Puis on va paramétrer le shader, la matière d’abord:
   Clic droit dessus → change type:
   **BDS**: ce sont les anciens shaders et les spéciaux
   **BDS Shading Network**: Ce sont les shaders les plus courant
   Ensuite, on va y mettre les textures ou couleurs:
   Clic droit sur le shader → execute → create diffuse texture, ect…
   On choisi ce qu’on veut. Si c’est une texture, on va écrire son
   chemin suivie de son nom dans “filename”.
   *NB*: VRAK est capricieux, il est important de faire un refresh
   souvent!
   Clic droit dans le vide de VRAK, refresh.
   Si le shader qu’on vient de créé ne s’affiche pas dans son onglet
   shader, il faut faire un refresh pour qu’il apparaisse.
d. **Faire un rendu**:
   \* En haut de la fenêtre VRAK, on a ces icones:
   La fleche simple permet de faire un de ces rendus suivants:
   Le draft, permet un calcul rapide mais avec une qualité d’image assez
   basse.
   Le RGB est plus long mais donne une image plus belle.
   Le renderMask, voir III/a.
   La double flèche permet de refaire le même type de rendu que le
   précédent.
   **NB**: On s’aperçoit à ce stade là qu’il est plutot utile de keyer
   sa camera. On peut rendre en perspective en faisant clic droit sur la
   théière de rendu du VrayFrameBuffer (VFB), mais ça plante au bout de
   quelques fois.
   \* Les erreurs les plus courantes sont déterminés par un code
   couleur:
   **Rouge**: le selector est vide ou il y a une erreur de frappe
   (manque une étoile, une parenthèse, ect … )
   **Violet/Magenta**: Le shader est vide: pas de color ni de texture
   **Blanc**: Il y a une erreur dans ASSIGN, peut être inversion de
   target et shader
   **Noir**: Il y a une erreur dans le chemin de la texture (un espace
   en trop, manque un slash, la texture n’existe pas, ect …)
e. **Sauver son VRAK**:
   \* Pour sauver le vrak de la scène de lighting, cad l’espace où on
   travaille son shading, il faut faire: clic droit dans l’espace de
   gauche et save.
   Si on reboot VRAK ([SMKS] → lighting → reboot VRAK), on aura toujours
   notre travail en cours.
   **A propos**: Le reboot de VRAK peut être utile. En effet, si on
   s’est trompé ou qu’on n’est pas content de son travail, plutôt que de
   s’embêter à supprimer ou déplacer certains fichiers dans VRAK (ce qui
   peut s’avérer fastidieux, voir …) On peut très bien rebooter notre
   VRAK. On aura alors le dernier VRAK enregistré.
   \* Sauver l’espace de travail est une chose, mais il va falloir
   sauver chaque VRAK dans son asset correspondant.
   Pour cela, on peut voir que si on sélectionne un asset dans la partie
   gauche, il y a des opinions blanches dans la partie droite.
   Quand c’est blanc, ça signifie qu’on n’a pas encore déplacé ces
   opinions de shading dans le fichier VRAK de l’asset.
   - On les selectionne toutes (juste les petites ampoules, pas le
   dossier qui les contient) clic droit → move opinions
   - On choisi le bon asset (on peut taper son nom dans la barre du haut
   pour le trouver plus facilement) et on move opinions.
   - clic droit dans la partie droite: refresh. On s’apperçoit que tout
   est devenu gris: ça signifie que les opinions ont été déplacés, mais
   il y a une étoile \* qui est apparue devant le nom de l’asset.
   L’étoile signifie que même si les opinions ont été déplacés, ils
   n’ont pas été enregistrés. Donc:
   -clic droit sur l’asset gris avec l’étoile → save
   Et en faisant un refresh à gauche, puis un à droite, l’asset
   réapparaît en gris, sans l’étoile. Il a bien été enregistré.

| **IV/**\ **Les petits truc utiles à savoir dans VRAK**\ **:**

a. **Supprimer**:
   Quand on veut supprimer un shader qu’on a créé. Il faut sélectionner
   ses opinions blancs et faire clic droit → delete. Refresh.
   Il faut le faire pour ASSIGN, pour SELECTOR et pour SHADER.
b. **Dupliquer**:
   *ex*: On a créé la tenue de Louis, et Theo a la même.
   On sélectionne l’asset “Louis”qu’on veut dupliquer et clic droit →
   duplicate:
   Là, il faut écrire le chemin du nouvel asset “Theo”:
   ASSET/ACTORS/EQUITATION/THEO
   Il va falloir drag-and-dropper SELECTORS et SHADERS dans ASSIGN.
   Il va aussi falloir changer “$Louis” en “$Theo” dans les selectors.
   On oublie pas de mover les opinions de Theo dans son fichier VRAK de
   shad render.
c. **Importer un VRAK déjà existant**:
   \* Ca peut être utile quand on utilise exactement le même décors par
   exemple, mais pas pour le même épisode. On peut ne rien changer du
   tout, ou bien si c’est en intérieur, on peut changer quelques colors.
   ATTENTION! Il faut que le nommage soit exactement le même!
   *ex*: “A” existe déjà, il a son VRAK qui existe.
    “B” est identique à “A” mais il n’a pas encore sont VRAK rempli.
   On va loader le fichier VRAK de “A” dans le supabrowser et copier son
   chemin.
   Dans la scène de lighting, dans VRAK, on affiche les opinions: clic
   droit dans le vide de la barre de menu de la fenêtre VRAK→ opinion:
   une colonne apparait à gauche, on refresh.
   Clic droit, add layer: on colle le chemin de “A” et on refresh.
   Dans la colonne du milieu, on sélectionne “A”.
   À droite, on sélectionne tous ses opinions (qui sont gris), et on les
   move vers “B”.
   On peut alors supprimer “A” des opinions, on refresh et on cache les
   opinions.
   On renomme correctement notre nouvel asset, mais attention! Il va
   falloir drag-and-dropper les SELECTORS et les SHADERS dans les
   ASSIGN, car ils ne portent pas le même nom.
d. **Renommer**:
   On peut renommer un asset ou un shader, en faisant clic droit →
   rename, mais il faut faire attention à deux choses:
   \* Un SHADER doit s’appeler comme son SELECTOR et comme son ASSIGN.
   \* Il faut penser à drag-and-dropper le SELECTOR et le SHADER renommé
   dans l’ASSIGN, pour qu’il les reconnaisse.
   **NB**: Quand on veut renommer et qu’une fenêtre nous en empêche,
   c’est qu’on a des opinions blancs à mover avant de renommer.

| **V/**\ **Les cas particuliers**\ **:**

a. **Les sports d’équipe**: (ce chapitre n’est peut être pas complet)
   \* Tout commence dans la scène de setup: on selectionne le root et on
   active le script correspondant, qui va créé 2 attributs: “team” et
   “number”.
   \* Shad Anim: on va vérifier sur quel switch se trouve le root (team
   A ou B), on sélectionne la modé qui va subir les changements de
   couleurs et on active le switch qui se trouve dans la shelf BDS
   render.
   \* IMPORTANT! Dans la scène de validation d’anim, il faut aller dans
   la shelf BDS Anim, et cliquer sur le bouton de valid.
   \* Il va falloir faire toutes les maps: chacune doit s’appeler ainsi:
   ASSET-RGB-4K_Team.number.jpg
   Sachant que A=0 et B=1 donc:
   *ex*: VOLLEYPLAYER-RGB-4K_0.1 ⇒ sera la map du joueur de l’équipe A
   portant le numéro 1.
   Dans le filename de VRAK en revanche, il faudra écrire:
   ASSET-RGB-4K_<TEAM>.<NUMBER>.jpg
b. **Les couleurs de peau**:
   Pour changer la couleur de peau d’un personnage, on va créer un
   shader “skin”.
   \* Dans son selector, on mettra \*body\*
   \* Son shader, il va se trouver dans change type → BDS → skin shader
   \* On peut alors choisir la couleur qui va overider celle par
   default.
c. **Les changement de texture de peau (maquillage, tâches de rousseur,
   ect …)**:
   \* Même principe que le précédent, sauf qu’on va chercher le
   skinCustomShader.
   \* Deux maps sont là de base, on remplace le chemin de la “head” par
   le chemin de la nouvelle texture.
d. **Le SkinFullCustomShader**:
   \* C’est encore le même principe, sauf que là, il va falloir
   renseigner les maps de TOUTES les parties du corps.
   \* On l’utilise rarement, souvent c’est lorsqu’un personnage se tache
   les mains ou les pieds.
e. **Le verre**:
   \* Le shader verre se trouve dans change type → BDS → Glass
   \* On a un petit utilitaire qui permet de changer la couleur de
   réfraction si on on veut un verre coloré.
f. **Utiliser un ancien asset (saison 1)**:
   Il se peut qu’en utilisant un asset de la saison 1, ça ne fonctionne
   pas correctement. Il faut alors aller dans la setup, sélectionner le
   groupe à la racine, aller dans l’attribute editor et ajouter un Vray
   User Attribute (s’il n’en a pas déjà un). On le nomme exactement
   comme son asset, en respectant minuscule et majuscule.
   On publish valid, ect…
   Cela va permettre à VRAK de reconnaître cet asset avec le “$” et de
   ne pas attribuer n’importe quoi à n’importe qui.
