#################
LES BASES DE VRAK
#################
by Max

*****
WTF ?
*****
Notre utilisation
=================

Vrak est un framework, un environnement, un contexte, un tool qu'on utilise pour beaucoup de chose.
On l'utilise au studio principalement pour:

	- assigner des shaders
	- assigner des propriétés
	- modifier le lighting d'une scène
	- découper des layers de rendus
	- définir des settings de rendus

Il est aussi possible de l'utiliser pour:

	- faire du shading
	- faire du lighting

Globalement Vrak est utilisé pour modifier le contenu d'une scène Maya au moment du rendu.

Techniquement
=============
VRAK
----
**VRak** c'est pour **VRay Assembly Kit**.
Ce n'est utilisable que pour une utilisation de VRay dans Maya.

Vrak c'est tout plein de bouts de codes python exécutés au moment du rendu, quand Maya donne la scène à VRay.
Pour utiliser Vrak (tous ces bouts de codes) on utilise SLOP

SLOP
----
**SLOP** c'est pour **Supamonks Layered Object Properties**
C'est le système dans lequel est utilisé Vrak.

L'interface GUI de Vrak c'est SLOP.
Le système d'opinions et de références c'est SLOP dans le pipe.

En général, on a pris l'habitude de ne parler que de Vrak pour l'ensemble Vrak/Slop.
C'est juste du jargon et du découpage mais c'est bien de comprendre qu'il y a 2 parties.

SLOP pourrait être utilisé avec d'autres moteurs de rendus, d'autres DCC.
VRak est complètement dédié à VRay.

.. tip ::
	| Il est possible d'ouvrir Vrak en dehors de Maya.
	| Attention, certaines entitées ne sont alors pas disponibles.

.. _vrak_ui:

***********
L'interface
***********
Pour ouvrir Vrak il y a donc 2 possibilités:

	- Directement depuis Kabaret

		.. image :: /images/vrak/v_open_k.gif

	- Depuis Maya

		.. image :: /images/vrak/v_open_m.gif

Depuis Maya, disponible en plus de l'action "Open Vrak" on trouve une action "Reboot Vrak"
	
	.. warning ::
		| **Dans Maya**, lorsqu'on ferme Vrak, tout ce qu'on y a fait reste.
		| Si on fait des choses dans Vrak, qu'on le ferme, puis qu'on l'ouvre, ce qu'on a fait avant sera toujours là **même si on a pas sauver la scène**.
		| Si on fait des choses, qu'on ferme Vrak, qu'on lance un "Reboot Vrak", ce qu'on a fait avant ne sera plus là.
		| Les références de Vrak sont faite lors du lancement de Vrak. Sans reboot les références ne sont pas mises à jour !

L'UI
====

| Différence d'UI depuis Maya ou Kabaret.
	| A droite l'interface qui s'ouvre depuis Maya.
	| A gauche celle qui s'ouvre depuis Kabaret.

	.. image :: /images/vrak/v_ui_compare.jpg

| Depuis Maya, tous les outils de la barre supérieure sont accessibles, contrairement à l'interface qui s'ouvre depuis Kabaret.

Le layout
---------
L'interface est répartie par défaut en 3 parties :

	- Les :ref:`menus et boutons <vrak_menus>`, dans la partie haute.
	- La :ref:`vue principale <vrak_main_view>`, à gauche qui sera la fenêtre de :ref:`travail <vrak_workflow>`.
	- La :ref:` vue "Item Opinion" <vrak_item_op>`, à droite, qui présente une liste des :ref:`opinions <vrak_opinions>` de la scène courante.

.. _vrak_menus:

Les menus et boutons
--------------------
| Dans la barre de menus et boutons, en haut de l'interface, se trouvent 2 lignes.
	
	.. image :: /images/vrak/v_top_menus.jpg

| La première ligne sera toujours la même.
| On y retrouvera toujours les 2 menus : "Session" et "Views"
	
	- Le premier servira simplement à quitter la session, donc à fermer Vrak (comme la croix habituelle sous Windows)
	- Le second permettra d'ouvrir 2 vues utilisées par les Devs : "EventLog" et "ConsoleView" qui forment à elles 2 l'équivalent Vrak du Script Editor de Maya.

| La seconde ligne comporte des boutons qui donneront accès à différents outils plus ou moins custom en fonction du projet.
| Les boutons qui seront toujours présents sont ceux là :

	.. image :: /images/vrak/v_buttons_render.jpg


	| Les 2 premiers, qui ressemble aux boutons de lecture et d'avance rapide des télécommandes, servent à lancer des rendus.
	| Ils permettent respectivement:

		- d'ouvrir une fenêtre qui liste les différents rendus disponibles pour être lancés
		- de relancer le même rendu que le précédent.

	| Le dernier bouton, lance un outils qui permet de gérer une sélection d'objet.

.. important::
	Les boutons de cette 2ème ligne ne sont jamais exactement à la même place !

| Les autres boutons permettent de lancer différents outils plus ou moins adaptés aux projets.

Les différentes vues
--------------------

.. _vrak_main_view:

La vue principale 
^^^^^^^^^^^^^^^^^
| Parmis les différentes vues, la principale est particulière.
| C'est le coeur de Vrak, sans elle rien ne fonctionne.

| Il s'agit d'une représentation de la scène Vrak sous forme d'arborescence.
| Comme l'outliner de Maya.

| Elle sera donc **toujours** présente !

	.. image :: /images/vrak/v_principal_view.jpg

| Tout en haut se trouvent quelques boutons.
	
	- "Scenes"

		Donne accès à 3 actions :

			- "New" pour repartir d'une nouvelle scène.
			- "Open" pour ouvrir une scène vrak existante.
			- "Save" pour sauvegarder la scène courante.

	- La case cochée

		Permettra de changer l'affichage du niveau de détail du contenu de la fenêtre.

	- Les boutons < et >

		Permettront de descendre ou remonter dans l'arborescence.

	- Le champ de chemin de l'arborescence

		| Vide au niveau du "root" de l'arbre.
		| Il affiche le chemin de la branche sélectionnée.

		.. tip::
			Il est possible de créer une branche de l'arborescence en tapant un nouveau chemin dans ce champ !

		.. image :: /images/vrak/v_principal_view_arbo.jpg

	- Le +
		| Permet de maximiser la fenêtre.
		| Il se transformera alors en - pour pouvoir minimiser la fenêtre et revenir à la vue initiale.

.. _vrak_item_op:

La vue "Item Opinions"
^^^^^^^^^^^^^^^^^^^^^^
| Elle est affichée par défaut.
| Contrairement à la :ref:`vue principale <vrak_main_view>`, il est possible de la fermer.
| Elle permet de voir la liste des différents :ref:`stages <vrak_stages>` et des :ref:`opinions <vrak_opinions>` qu'ils contiennent.

	.. image :: /images/vrak/v_item_opinions_view.jpg

| Tout en haut se trouvent quelques boutons.

	- "Item Opinions"

		Donne accès à 2 actions cases à cocher (cochées par défaut):

			- "One Line Details" force l'affichage des détails sur une seule ligne.
			- "Show Branch" affiche toutes les opinions de tous les enfants de la sélection.
				Décochée, la vue n'affichera que les opinions du chemin sélectionné.

			.. warning ::
				| Comme pour beaucoup d'actions réalisées dans Vrak, il y pour le moment un problème de Refresh.
				| L'utilisation de ces cases donne une erreur, mais l'effet sera visible après avoir rafraichit la vue.

			.. tip ::
				| Pour rafraichir la vue, il faut faire un clic droit dans une partie vide de la fenêtre pour accéder à l'action Refresh.
				| Pour cela, il faudra refermer tous les stages de la scène en cliquant sur les - à côté desnoms de fichier.
				| Il y aura alors assez d'espace pour faire un refresh de la vue.

	- Les 3 boutons + > et X

		Permettront respectivement de :

			- maximiser la vue puis le - permettra de la minimiser
			- l'ouvrir dans une fenêtre indépendante qui pourra alors être manipulée comme les fenêtre de :ref:`Kabaret <kabaret>`.
			- la fermer

Les fenêtres
^^^^^^^^^^^^
| D'autres fenêtres peuvent être ouvertes.
| Pour ouvrir une fenêtre qui n'est pas actuellement disponible dans l'interface, il suffit de faire un clic droit dans la partie haute de l'interface, au niveau des :ref:`menus et boutons <vrak_menus>`.

	.. image :: /images/vrak/v_views.gif

| Le menu disponible dans le clic droit est découpé en 2 sections:

	- La partie haute pour les vues
		On y trouve les fenêtres disponibles, comme la :ref:`vue "Item Opinions" <vrak_item_op>`.
	- La partie basse pour les tools
		On y trouve les différents sets de tools disponible.

.. _vrak_opinions_view:

La vue "Opinions"
^^^^^^^^^^^^^^^^^
| Pour accéder à cette vue qui n'est pas affichée par défaut, il faut passer par le menu du clic droit dans la partie haute de l'interface.

	.. image :: /images/vrak/v_opinion_view.gif

| Cette vue présente la liste des :ref:`opinions <vrak_opinions>` de toute la scène, :ref:`l'ordre <vrak_order>` du bas vers le haut.

.. _vrak_scenes:

**********
Les scènes
**********
| Les scènes Vrak c'est le nerf de la guerre.
| Elles sont représentées en arborescence dans la :ref:`vue principale <vrak_main_view>`.

	.. image :: /images/vrak/v_arbo.jpg

| Sur l'image ci-dessus, on peut comment est représentée une scène Vrak.
| La deuxième ligne nous donne plusieurs infos.

	- "[demo_arbo.vrak]" c'est le nom de la scène vrak courante.
			| Cette première cellule définie aussi une première colonne. 
			| Cette colonne est la représentation principale de l'arborescence. 
			| On accède par cette première colonne à l'arborescence que l'on peut déplier ou replier.
			| Il s'agit d'une arborescence du même type que ce qu'on peut avoir dans l'explorateur windows ou l'outliner de maya.
			| Les éléments avec l'icône bleu étant l'équivalent de dossiers windows, des morceaux du chamin que l'on peut voir sur la première ligne.
			| Ceux avec des icônes différentes sont des entités.
			| Chaque entité peut avoir des paramètres.
	- "Value", indique que la colonne concernée correspond à la valeur du paramètre de l'entité concernée.
	- "Kind", indique le type de l'entité
	- "Nb Op.", le nombre d'opinions du paramètre ou de l'entité concernés.

| Pour pouvoir utiliser Vrak, la seule chose nécessaire est d'avoir dans l'arborescence une entité de type "Render".
| Une scène ne contenant qu'une entité de type "Render" permettra alors de faire un rendu de la scène maya, sans aucun override de la part de Vrak.
| Le résultat sera donc le même avec ou sans Vrak.

.. _vrak_entities:

************
Les entitées
************
Définition
==========
| Une entitée, c'est un chemin auquel est attribué un type.

Création d'entitées
===================
| De base dans Vrak, l'idée c'est que l'on peut écrire un chemin, appuyer sur la touche "Entrée", ce qui le fera apparaitre dans l'arborescence.

	.. tip ::
		| Pour que l'opération, il faut créer le premier bout du chemin séparément. 
		| Donc pour le chemin "Path/to/Render", il faut taper "Path" puis faire "Entrée", ensuite on peut faire tout le reste du chemin.
		| Sans ça il y a des problèmes de rafraichissement.

|

	.. image :: /images/vrak/v_write_path.gif

|

	.. warning ::
		| Un chemin dont l'extremité n'a pas de type ne sert à rien.
		| Tel que présenté dans le gif ci-dessus, le chemin disparaitra au premier rafraîchissement.
		| Il n'y a que les entitées (chemin avec type attribué) qui puissent exister dans Vrak.

|
	
	.. image :: /images/vrak/v_add_type.gif

| Pour créer une entitée il est aussi possible d'utiliser la commande "Create Here", disponible dans le clique droit.

	.. image :: /images/vrak/v_create_entity.gif

| Le premier champs permet de filtrer les types d'entités disponibles.
| Le dernier permet de définir le nom de l'entité.
	
	.. tip ::
		| Si aucun nom n'est entré dans le champs "Item Name", le nom de l'entité sera celui de son type.

	.. tip ::
		| L'entitée est créée à partir de du chemin sélectionné.

	.. tip ::
		**Entitée** = **Item**

La manipulation des entitées
============================
Renommer
--------
| Il est possible de renommer une entitée.
| Lors du renommage de l'entitée, c'est sur tout le chemin que l'action s'opère.
| La renommer permettra donc de la déplacer dans l'arborescence.

	.. image :: /images/vrak/v_entity_rename.gif

.. attention::
	| Renommer une entitée en lui donnant un nom qui existe déjà viendra mettre ses opinions par dessus la première.
	| Par exemple

.. _vrak_params:

**************
Les paramètres
**************
Définition
==========
| Une entitée peut avoir des paramètres.
| Elle peut aussi ne pas en avoir, ça signifie simplement qu'elle n'est pas paramétrable.

Manipulation
============
| Tous les paramètres d'une entitée peuvent être édités dans la colonne "Value".
| Ils peuvent avoir des types différents comme:
	- des "strings" (du texte)
	- des nombres entiers ou flottant.
	- des booléens (True vs False)
	- des valeurs particulières généralement des strinfgs spécifiques comme "VSL", "AUTO" etc...

.. _vrak_opinions:

************
Les opinions
************

.. attention ::
	Ceci est une notion fondamentale de Vrak !

Définition
==========
| Créer une entitée, c'est donner un type à un chemin.
| Attribuer un type à un chemin ou une valeur à un paramètre, c'est créer une opinion.

| Quand on créer une entitée, en fait on créé une opinion qui dit : "Ce chemin a ce type"
| Quand on change la valeur d'un paramètre, on créé une opinion qui dit : "Ce paramètre, de ce chemin, a cette valeur"

	.. image :: /images/vrak/v_opinion.jpg

| En regardant dans la :ref:`vue "Item Opinions" <vrak_item_op>`, on peut voir les opinions de la branche courante et de ses enfants.

Exemple
=======
| Dans l'image ci-dessus on peut donc voir que l'entitée "RenderName" a 2 opinions.
	- La première une opinion de type "SetKind", elle correspond l'attribution du type "Render" au chemin "Path/to/RenderName".
	- La second de type "Set", elle correspond à l'attribution de la valeur 1 au paramètre "Camera".

| Dans les détails de ses opinions on peut voir le code qui correspond à l'opinion.

	.. image :: /images/vrak/v_opinions.jpg

Utilisation
===========
| Dans l'image ci-dessus on peut voir que la valeur du paramètre "Camera" est maintenant 2.
| Dans la :ref:`vue "Item Opinions" <vrak_item_op>`, on voit qu'il y a toujours une opinion de type "Set" dont la valeur est à 1 pour la camera.
| On en voit une deuxième dont la valeur est à 2.

	.. important ::
		| Tout ce qui est fait dans Vrak créer une opinion, toutes les opinions restent visibles depuis les vues :ref:`"Item Opinions" <vrak_item_op>` et :ref:`"Opinions" <vrak_opinions_view>`.
		| Il est donc possible de connaître tout l'historique d'une entitée depuis ces vues.

	.. warning ::
		| Il n'y a pas Ctrl+Z dans Vrak !

.. _vrak_del_op:
| Depuis une vue d'opinion, il est possible d'en supprimer une ou plusieurs à la fois.

	.. image :: /images/vrak/v_delete_opinion.gif

|

	.. tip ::
		| Pour pallier le manque de Ctrl+Z, finalement, il suffit de supprimer les opinions qui ne plaisent pas ^^ !

	.. warning ::
		Une fois l'opinion supprimer, il n'y a pas de retour possible.

.. _vrak_order:

Notion importante : l'ordre
===========================

.. attention ::
	| Dans Vrak l'ordre est important !
	| Pour deux opinions qui attribuent une valeur au même paramètre, c'est la dernière appliquée qui l'emporte !
	| C'est donc la dernière opinion qui apparait dans le champs "Value" de la :ref:`vue principale <vrak_main_view>`.
	| Dans la :ref:`vue "Item Opinions" <vrak_item_op>`, les opinions se lisent du haut vers le bas, la plus basse l'emporte.
	| Dans la :ref:`vue "Opinions" <vrak_opinions_view>`, les opinions se lisent du bas vers le haut, c'est la plus haute, la plus récente, qui l'emporte.

	| L'ordre est aussi important pour les Entitées.
	| Les entitées sont exécutées du haut vers le bas !
	| Les entitées sont rangées dans l'ordre suivant :
		| 0-9 puis A-Z puis a-z

		.. image :: /images/vrak/v_entity_order.jpg

.. _vrak_stages:

**********
Les stages
**********
Définiton
=========
| Un stage, c'est le contenu d'un fichier Vrak.
| Une scène Vrak c'est un stage.

.. _vrak_include:

Include
=======
| Un stage peut contenir plusieurs stages, on parle alors d'includes.

| En gros un stage dans un stage c'est une référence.

.. important ::
	| Un include, c'est aussi une opinion !

.. attention ::
	| Là encore l'ordre est important !
	| Plusieurs includes peuvent s'appliquer aux mêmes paramètres ! 
	| C'est le dernier appliqué qui l'emporte !

	| Vous pourrez voir :ref:`ici <vrak_pipe>` comment sont utilisés les include dans le système de référence du pipe pour Vrak.

Faire un include
================
| Pour faire un include depuis l'interface de Vrak, il n'y a qu'un moyen.
| Il faut ouvrir la :ref:`vue "Opinions" <vrak_opinions_view>`

| Ensuite, dans une partie vide l'interface, dans le menu du clic droit sera disponible l'action "AddLayer".

	.. image :: /images/vrak/v_addlayer.gif

| Une nouvelle opinion s'ajoute à la liste de la :ref:`vue "Opinions" <vrak_opinions_view>`.
| Cette opinion est de type "Ref".
| Sous cette opinion se trouvent toutes les opinions contenues dans ce stage.

| Les opinions contenues viendront overrider celle se trouvant en dessous dans la lsite de la :ref:`vue "Opinions" <vrak_opinions_view>`.

| On retrouvera également les opinions de cet include dans la liste de la :ref:`vue "Item Opinions" <vrak_item_op>`.
| A la différence de la :ref:`vue "Opinions" <vrak_opinions_view>`, les opinions de la :ref:`vue "Item Opinions" <vrak_item_op>` sont d'abord classées par stage.
| Les stages eux mêmes sont classé de manière un peu esotérique même si les les opinions contenues sont dans :ref:`organisées <vrak_order>`.

	.. image :: /images/vrak/v_opinion_order_views.jpg

| Dans l'image ci-dessus et celles qui suivent, on peut voir, concernant le paramètre "camera" de l'entitée "Path/to/Entity/Render":

	- Dans la :ref:`vue principale <vrak_main_view>`, la valeur du paramètre à 2.

		.. image :: /images/vrak/v_opinion_order_views_mv.jpg

	- Dans la :ref:`vue "Item Opinions" <vrak_item_op>`:

			- les stages qui ne sont pas sauvegardés en **rouge**.
			- les stages en références qui sont **grisés**.
			- les opinions réparties dans les 2 stages. Avec une opinion qui met la valeur à 3 dans la ref et 2 opinions dans le stage courant avec des valeurs à 1 et à 2.

			.. image :: /images/vrak/v_opinion_order_views_iop.jpg

	- Dans la :ref:`vue "Opinions" <vrak_opinions_view>`, en lisant de bas en haut, on peut lire que le paramètre a d'abord été mis à 1 dans la scène courante (ligne 3), puis à 3 depuis la référence (ligne 5) et enfin à 2 (ligne 7, tout en haut).

		.. image :: /images/vrak/v_opinion_order_views_ov.jpg

| On remarque donc que la valeur actuelle du paramètre que l'on voit dans la :ref:`vue principale <vrak_main_view>` vient du stage courant ("UntitledStage", scène vide, jamais sauvegardée).
| On remarque dans la :ref:`vue "Opinions" <vrak_opinions_view>` que les opinions se chaevauchent entre la scène courante et la ref.

.. important ::
	| Dans cet exemple, si je fais un :ref:`"Delete Opinions" <vrak_del_op>` sur celle qui met la valeur à 2, alors la valeur du paramètre "camera" sera **3** !

.. _vrak_pipe:

*****************
VRAK dans le pipe
*****************
Petit Rappel
============
| Un fichier Vrak peut prendre en référence un autre fichier. On appelle ça un :ref:`"Include" <vrak_include>`.
| Comme dans tout le système de Vrak, un include est une opinion.
| Comme dans tout le système de Vrak, les opinions s'exécutent dans l'ordre, chaque nouvelle opinion overridant les plus anciennes.

Le pipe dans la Bank
====================
Il y a 3 fichiers au projet:

	- Default Shading
	- Default Lighting
	- Render Settings

Un fichier par asset :
	
	- Shad Render

Le pipe dans les films
======================
Puis:

	- Film (Vrak du film)
	- Override (Vrak de la sequence)

| Dans chaque plan, le fichier "Casting" va faire un include pour tous les fichier vrak nécessaires.
| Il va faire les includes dans cet ordre :

	1. Les 3 vraks du projet
	2. Les Shad Renders de tous les assets présents dans le casting du plan.
	3. Le Vrak du film
	4. Le Vrak de la sequence du plan concerné

| En suivant cet ordre, les opinions du fichier vrak de la sequence écraseront celles de tous les fichiers précédement inclus (donc ceux du projet ou des shad render par exemple).
| Ensuite le fichier Casting est pris en référence dans le fichier du plan.
| Cela permettra de regénérer le fichier Casting en cas de changement du casting du plan sans perdre les opinions du plan.

.. attention::
	| Les vrak de shading des assets sont pris dans l'ordre alphabétique.
	| Les opinions qu'ils contiennent peuvent s'overrider les unes aux autres.

.. _vrak_workflow:

***********
Le workflow
***********
| Comme expliqué dans la définition d'une :ref:`scène <vrak_scenes>`, l'élément le plus important de Vrak est l'entitée de type "Render".
| Le principe c'est d'utiliser Vrak pour faire du rendu, en découpant et modifiant une scène maya dans la même logique que les renders layers et leurs overrides dans maya.

Quelques entitées de bases
==========================
Render
------
Les entitées de type "render" ont pour paramètres par défaut:

	- "camera" : qui attend le nom d'une caméra de la scène pour forcer le rendu Vrak depuis celle-ci.

		.. attention::
			| Par défaut, la vue rendu sera toujours la vue courante !

	- "entities" : c'est **LE** paramètre important de cette entitée.
	- "first" : paramètre obsolète
	- "last" : paramètre obsolète
	- "verbose" : c'est un paramètre booléen. Par défaut en mode False, le mode True permettra d'accéder à plus de prints dans le script editor ou le log de rendu. Attention, le temps d'exécution de Vrak sera plus long !

.. _vsl:

***
VSL
***

| VSL pour "Vrak Shading Language".
| C'est le système de shading de Vrak le plus utilisé au studio.

Le principe
===========
| L'idée c'est de faire des shading network dans maya et d'exporter ça pour que ce soit utilisé dans Vrak.
| Tout le shading network est donc transféré depuis maya, dans un fichier ".vsl".
| Ensuite à l'aide de certaines entitées Vrak, on peut utiliser et modifier le contenu de ces fichiers.

Préparer le shading network
===========================
| On doit donc faire le shading dans une scène dédiée en utilisant des nodes maya/vray.

	.. warning::
		| Certains nodes ne sont pas compatibles avec le système:
			- le remapHSV
			- le remapValue

		| Globalement, les nodes maya qui utilisent des courbes d'ajustement ne sont pas compatibles.

	.. tip::
		| Pour pallier ces incompatibilités, il est possible d'utiliser d'autres nodes.
		| Par exemple, le node de color correct disponible depuis le menu "Create/Vray/Create From Vray plugin/Texture" proposent les fonctionnalités nécessaires pour obtenir la majeure partie des effets désirés.

Export de shading network
=========================
| Pour exporter le surfacing d'un asset il faut avoir dans la scène Vrak, une entitée de type "VSL_Export".
| Cette entitée a 3 paramètres:
	
	- "material_shape_pattern" : pattern de sélection des shapes dont les matériaux seront exportés.
	- "settings_pattern" : pattern de sélection des settings qui seront exportés.
	- "vsl_path" : le chemin du fichier VSL

Exposer les paramètres utiles
=============================
| Exporter directement un shader est la première étape pour bien vérifier que le shading network est compatible avec le système.
| Une fois que le shading est prêt, il est possible d'exposer certains paramètres pour qu'ils soient accessible depuis Vrak.
| Par défaut, aucun paramètre n'est exposé, donc aucun ne sera accessible facilement depuis Vrak, donc depuis les scènes de lighting par exemple.

| Pour exposer un paramètre 
